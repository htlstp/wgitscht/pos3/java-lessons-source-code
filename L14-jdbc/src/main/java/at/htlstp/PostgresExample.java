package at.htlstp;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgresExample {


    private  Connection connection;

    public PostgresExample() throws SQLException {
        connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres",
                "nodepg","nodepgpw");

    }

    public void getMetaData() throws SQLException {
        DatabaseMetaData meta = connection.getMetaData();
        System.out.println("database version = " + meta.getDatabaseMajorVersion());
    }


    public void getProducts() throws SQLException {
        var stmt = connection.createStatement();
        var resultSet = stmt.executeQuery("select * from products");
        while (resultSet.next()) {
            System.out.println(resultSet.getString(2));
        }
    }
}

