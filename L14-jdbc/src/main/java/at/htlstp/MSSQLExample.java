package at.htlstp;

import java.sql.*;

public class MSSQLExample {


    private final Connection connection;

    MSSQLExample() throws SQLException {
        connection = DriverManager.getConnection(
                "jdbc:sqlserver://localhost;database=werner;trustServerCertificate=true;",
                "gits", "geheim");
    }


    public void getTables() throws SQLException {
        DatabaseMetaData meta = connection.getMetaData();
        ResultSet rs = meta.getTables(null, null, null,
                new String[]{"TABLE"});

        while(rs.next()) {
            System.out.println(rs.getString(2));
        }
    }
}
