
create table users(
                      id serial primary key ,
                      first_name varchar(30),
                      last_name varchar(30)
);


create table staff (
                       id serial,
                       department varchar(30),
                       repsonsiblity varchar(30),
                       user_id int references users(id)
);

insert into users(first_name, last_name) VALUES ('werner','gitschthaler');
INSERT INTO users (first_name, last_name) VALUES
                                              ('John', 'Doe'),
                                              ('Jane', 'Doe'),
                                              ('Jim', 'Beam'),
                                              ('Jack', 'Daniels'),
                                              ('Johnny', 'Walker'),
                                              ('Jill', 'Valentine'),
                                              ('Claire', 'Redfield'),
                                              ('Chris', 'Redfield'),
                                              ('Leon', 'Kennedy'),
                                              ('Ada', 'Wong'),
                                              ('Albert', 'Wesker'),
                                              ('Jesse', 'Pinkman'),
                                              ('Walter', 'White'),
                                              ('Saul', 'Goodman'),
                                              ('Mike', 'Ehrmantraut'),
                                              ('Gus', 'Fring'),
                                              ('Hank', 'Schrader'),
                                              ('Skyler', 'White'),
                                              ('Marie', 'Schrader'),
                                              ('Lydia', 'Rodarte-Quayle');
insert into staff(department, repsonsiblity, user_id) VALUES ('justice','judge',1);
insert into staff(department, repsonsiblity, user_id) VALUES
                                                          ('IT', 'Security', 2),
                                                          ('Human Resources', 'Recruitment', 3),
                                                          ('Human Resources', 'Employee Relations', 4),
                                                          ('Finance', 'Accounting', 5),
                                                          ('Finance', 'Payroll', 6),
                                                          ('Marketing', 'Social Media', 7),
                                                          ('Marketing', 'Public Relations', 8),
                                                          ('Operations', 'Logistics', 9),
                                                          ('Operations', 'Inventory Management', 10),
                                                          ('Sales', 'Direct Sales', 11),
                                                          ('Sales', 'Customer Service', 12),
                                                          ('Executive', 'CEO', 13),
                                                          ('Executive', 'CFO', 14),
                                                          ('Research and Development', 'New Products', 15),
                                                          ('Research and Development', 'Improvements', 16),
                                                          ('Legal', 'Contracts', 17),
                                                          ('Legal', 'Compliance', 18),
                                                          ('Education', 'Training', 19),
                                                          ('Education', 'Development', 20);


update users set first_name = 'Werner' where id = 1

select * from users;