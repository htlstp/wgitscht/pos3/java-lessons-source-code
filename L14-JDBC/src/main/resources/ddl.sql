use master;
go;
create database gits;
go;
create login wgitscht WITH PASSWORD = 'wgitscht';
go;
create user wgitscht for LOGIN wgitscht;
go;

use gits;

ALTER ROLE [db_owner] ADD MEMBER wgitscht;

create schema javalesson;
go;
create TABLE  javalesson.firma (
                                   id BIGINT IDENTITY (1,1),
                                   name VARCHAR(30),
                                   industry VARCHAR(20)
)

create TABLE  javalesson.employee (
                                      id BIGINT IDENTITY (1,1) PRIMARY KEY,
                                      first_name VARCHAR(30),
                                      last_name VARCHAR(20),
                                      salary DECIMAL(9,2) NOT NULL
)