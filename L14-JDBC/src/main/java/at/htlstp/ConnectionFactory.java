package at.htlstp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    public static Connection getPostgresCon() throws SQLException {
        String URL = "jdbc:postgresql://localhost:5432/postgres";
        String USER = "nodepg";
        String PASSWORD = "nodepgpw";
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

    public static Connection getMSSQLCon() throws SQLException {
        String URL = "jdbc:sqlserver://localhost;trustServerCertificate=true;database=gits";
        String USER = "werner";
        String PASSWORD = "secret";
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

}
