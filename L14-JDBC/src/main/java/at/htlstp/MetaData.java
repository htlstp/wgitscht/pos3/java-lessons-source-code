package at.htlstp;

import java.sql.*;

public class MetaData {


    private Connection con;
    public MetaData(Connection jdbcUrl) {
        this.con = jdbcUrl;
    }

    public static void main(String[] args) throws SQLException {
        MetaData sirs = new MetaData(ConnectionFactory.getPostgresCon());
        sirs.printTableMetadataRS();
        sirs.printTableMetadata();
    }

    private void printTableMetadata() throws SQLException {
        try(ResultSet rs = con.getMetaData().getTables("","", "", new String[]{"TABLE"})) {;
            printRowData(rs);
        };
    }

    public void printTableMetadataRS() throws SQLException {
        try( PreparedStatement ps = con.prepareStatement("SELECT * FROM users",
                ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE)) {
            ResultSet rs = ps.executeQuery();
            printRSMetadata(rs);
        }
    }

    private static void printRSMetadata(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
            System.out.printf("%s:%d %d%n",metaData.getColumnName(i),
                    metaData.getColumnType(i),metaData.isNullable(i));
        }
        System.out.println();
    }

    private static void printRowData(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        while(rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.printf("%s:%s ",metaData.getColumnName(i), rs.getString(i));
            }
            System.out.println();
        }
    }
}

