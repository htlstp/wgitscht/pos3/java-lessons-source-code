package at.htlstp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetsDemo {

    private static final String SQL = "SELECT * from users";
    private final Connection con;

    public ResultSetsDemo(Connection con) {
        this.con = con;
    }

    public static void main(String[] args) throws SQLException {
        ResultSetsDemo resultSetsDemo = new ResultSetsDemo(ConnectionFactory.getPostgresCon());

        resultSetsDemo.printAllRows();
        resultSetsDemo.insertRowDemo();
        resultSetsDemo.printAllRows();
    }


    private void printAllRows() throws SQLException{

        try(PreparedStatement ps = con.prepareStatement(SQL)) {
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        }
    }

    private void insertRowDemo() throws SQLException {
        try(PreparedStatement ps = con.prepareStatement(SQL,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
            ResultSet rs = ps.executeQuery();
            rs.moveToInsertRow();
            rs.updateString("first_name", "Christoph");
            rs.updateString("last_name", "Schreiber");
            rs.insertRow();
        }
    }
}
