package at.htlstp;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        PostgresExample postgres = new PostgresExample();
//        postgres.getMetaData();
        postgres.getProducts();

        MSSQLExample example = new MSSQLExample();
        example.getTables();
    }
}