package at.htlstp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CLIDbBrowser {

    private static Connection connection;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            connection = ConnectionFactory.getPostgresCon();
            DatabaseMetaData dbmd = connection.getMetaData();
            System.out.println("forw = " + dbmd.supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY));
            System.out.println("scroll insens = " + dbmd.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE));
            System.out.println("scroll sensitive = " + dbmd.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE));
            System.out.println("other updates visible = " + dbmd.othersUpdatesAreVisible(ResultSet.TYPE_SCROLL_SENSITIVE));
            System.out.println("other inserts visible = " + dbmd.othersInsertsAreVisible(ResultSet.TYPE_SCROLL_SENSITIVE));


            while (true) {
                List<String> tables = listTables();
                System.out.println("Choose a Table to select: (0 exits)");
                int choice = Integer.parseInt(scanner.nextLine()) - 1;

                if (choice >= 0 && choice < tables.size()) {
                    browseTableData(tables.get(choice));
                } else if (choice == -1) {
                    System.out.println("exiting!");
                    break;
                } else {
                    System.out.println("illegal choice!");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static List<String> listTables() throws SQLException {
        List<String> tables = new ArrayList<>();
        DatabaseMetaData metaData = connection.getMetaData();
        try(ResultSet rs = metaData.getTables(null, null, null, new String[]{"TABLE"})) {
            while (rs.next()) {

                tables.add(rs.getString("TABLE_SCHEM") + "." + rs.getString("TABLE_NAME"));
            }
        }


        for (int i = 0; i < tables.size(); i++) {
            System.out.println((i + 1) + ". " + tables.get(i));
        }
        return tables;
    }

    private static void browseTableData(String tableName) throws SQLException {
        String sql = "SELECT * FROM "+ tableName + " order by id";
        try (PreparedStatement stmt = connection.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)
           ) {
            ResultSet rs = stmt.executeQuery();
            int currentPage = 0;
            while (true) {
                if (!showPage(rs, currentPage)) {
                    System.out.println("no more pages. 'exit' to return");
                }
                System.out.println("type 'n' for next page and 'p' for previous");
                String line = scanner.nextLine();
                if ("n".equals(line)) {
                    if (!rs.isAfterLast()) {
                        currentPage++;
                    } else {
                        System.out.println("already on last page");
                    }
                } else if ("p".equals(line)) {
                    if (currentPage > 0) {
                        currentPage--;
                    } else {
                        System.out.println("already on first page");
                    }
                } else if ("exit".equals(line)) {
                    break;
                }
            }
            rs.close();
        }
    }

    private static boolean showPage(ResultSet rs, int page) throws SQLException {
        int rowsPerPage = 2;
        int startRow = page * rowsPerPage + 1;
        if (!rs.absolute(startRow)) {
            return false;
        }
        for (int i = 0; i < rowsPerPage && !rs.isAfterLast(); i++) {
            printRow(rs);
            rs.next();
        }
        return true;
    }

    private static void printRow(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
            System.out.printf("%s:%s ",metaData.getColumnName(i), rs.getString(i));
        }
        System.out.println();
    }

}
