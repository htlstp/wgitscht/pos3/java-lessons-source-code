package at.htlstp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class SimpleDBQuery {

    public static final String URL = "jdbc:sqlserver://localhost;trustServerCertificate=true;database=gits";

    /**
    private static final JdbcDataSource dataSource = new JdbcDataSource();

    static {
        dataSource.setURL("jdbc:h2:./foobar");
    }
     **/

    static Connection connection;

    static {
        try {
            //connection = DriverManager.getConnection("jdbc:h2:./foobar");
            connection = DriverManager.getConnection(URL,"werner","secret");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public SimpleDBQuery() throws SQLException {
    }

    public static void main(String[] args) throws SQLException {
        //Connection connection = DriverManager.getConnection(URL,"werner","secret");
        DatabaseMetaData dbmd = connection.getMetaData();
        System.out.println("dbmd:driver version = " + dbmd.getDriverVersion());
        System.out.println("dbmd:driver name = " + dbmd.getDriverName());
        System.out.println("db name = " + dbmd.getDatabaseProductName());
        System.out.println("db ver = " + dbmd.getDatabaseProductVersion());
        System.out.println("forw = " + dbmd.supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY));
        System.out.println("scroll insens = " + dbmd.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE));
        System.out.println("scroll sensitive = " + dbmd.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE));

        // features
        System.out.println("supports full outer joins = " + dbmd.supportsFullOuterJoins());

        /*
        try(ResultSet resultSet = dbmd.getTables(null, "javalesson", null,
                new String[]{"TABLE"})){
            while(resultSet.next()) {
                System.out.println(resultSet.getString("TABLE_NAME"));
            }
        }


        try(Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * from javalesson.firma")) {
            while (rs.next()) { // durch result set loop

            }
        }
*/
        /*
        try(Statement insertStatement = connection.createStatement()) {
          int rowsAffected = insertStatement.executeUpdate("""
                  INSERT INTO javalesson.firma
                   (name, industry) 
                   VALUES 
                   ('gitsch-consult','Consulting')""");
        }

        try(Statement createStatement = connection.createStatement()) {
            int allOk = createStatement.executeUpdate("""                  
                    create TABLE  javalesson.employee (
                    id BIGINT IDENTITY (1,1) PRIMARY KEY,
                    first_name VARCHAR(30),
                    last_name VARCHAR(20),
                    salary DECIMAL(9,2) NOT NULL
                    )
                  """);
            assert allOk == 0;
        }

 */
        try(PreparedStatement statement = connection.prepareStatement(""" 
                SELECT first_name as FiRst_NAME, last_name, salary
                FROM javalesson.employee
              """, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ) {
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            System.out.printf("%20s |%20s | %8.2f%n",
                    resultSet.getString("first_name"),
                    resultSet.getString("LasT_NamE"),
                    resultSet.getDouble("salary"));
            resultSet.next();
            System.out.printf("%20s |%20s | %8.2f%n",
                    resultSet.getString("first_name"),
                    resultSet.getString("LasT_NamE"),
                    resultSet.getDouble("salary"));
            resultSet.previous();
            System.out.printf("%20s |%20s | %8.2f%n",
                    resultSet.getString("first_name"),
                    resultSet.getString("LasT_NamE"),
                    resultSet.getDouble("salary"));
            resultSet.last();
            System.out.printf("%20s |%20s | %8.2f%n",
                    resultSet.getString("first_name"),
                    resultSet.getString("LasT_NamE"),
                    resultSet.getDouble("salary"));
        }
/*
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("""
                SELECT * 
                FROM javalesson.person 
                WHERE first_name LIKE '%%%s%%'"""
                    .formatted("' OR '1' LIKE '1"));
            // result set verarbeitung..
            // return
            while(resultSet.next()) {
                System.out.println(resultSet.getString(1));
                System.out.println(resultSet.getString(2));
            }
        }
    */

    }

}
