package at.ac.htlstp.gits;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.SQLOutput;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;

public class OptionalFunctional {

    public static void main(String[] args) throws SocketException {

        //new OptionalFunctional().withFunctional();
        new OptionalFunctional().withFunctional2();
    }




    private static void optionalPrimitives(){
//        OptionalDouble exp =
//                OptionalDouble.of( 2 ).stream().map( Math::exp ).findFirst();
//        System.out.println(exp.isPresent());
        OptionalInt optionalInt = OptionalInt.empty();
        System.out.println(optionalInt.isPresent());
    }

    private void withOptional() throws SocketException {
        Optional<NetworkInterface> networkInterface =
                Optional.ofNullable(NetworkInterface.getByIndex(3));
        if(networkInterface.isPresent()) {
            Optional<String> name = Optional.ofNullable(networkInterface.get().getName());
            if(name.isPresent()) {
                System.out.println(name.get().toUpperCase());
            }
        }
    }
    private void withFunctional() throws SocketException {
        Optional<NetworkInterface> networkInterface =
                Optional.ofNullable(NetworkInterface.getByIndex(2));
        networkInterface.ifPresent(o -> {
            Optional<String> name = Optional.ofNullable(networkInterface.get().getName());
            name.ifPresent(n -> System.out.println(n.toUpperCase()));
        });
    }

    private void withFunctional2() throws SocketException {
        Optional.ofNullable(NetworkInterface.getByIndex(2))
               .map(networkInterface -> networkInterface.getDisplayName())
               .map(String::toUpperCase)
                .map(s -> s.length())
                .ifPresent(System.out::println);

    }

    private void withNullCheck() throws SocketException {
        NetworkInterface networkInterface = NetworkInterface.getByIndex(2);
        if (networkInterface != null) {
            String displayName = networkInterface.getDisplayName();
            if (displayName != null)
                System.out.println(displayName.toUpperCase());
        }
    }
}
