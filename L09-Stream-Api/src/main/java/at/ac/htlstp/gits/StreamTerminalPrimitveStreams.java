package at.ac.htlstp.gits;

import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;

public class StreamTerminalPrimitveStreams {
    public static void main(String[] args) {
        getMax();
        getSummary();
    }

    private static void getMax() {
        int max  = IntStream.range(1,5).max().orElse(Integer.MIN_VALUE);
        double avg = IntStream.range(1,5).average().orElse(0.0);
        int sum = IntStream.range(1,5).sum();


    }

    private static void getSummary() {
        IntSummaryStatistics intSummaryStatistics = IntStream.range(1, 5).summaryStatistics();
        int max = intSummaryStatistics.getMax();
        int min = intSummaryStatistics.getMin();
    }
}
