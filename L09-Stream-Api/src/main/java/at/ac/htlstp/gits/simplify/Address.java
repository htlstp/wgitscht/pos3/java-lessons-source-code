package at.ac.htlstp.gits.simplify;

public record Address(String city, int zip) {

    public Address(String city ){
        this(city, 0);
    }

    public Address withCity(String name){
        return new Address(name, zip());
    }
}
