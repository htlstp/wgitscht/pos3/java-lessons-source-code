package at.ac.htlstp.gits;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTerminalCollectors {

    public static void main(String[] args) {
        collectors();
        joining();
        terminalConsumesString();
        peek();
        reduce();
        toMap();
        groupingBy();
    }


    private static void joining(){
        String s = Stream.of(1, 2, 3, 4)
                .map(Object::toString)
                .collect(Collectors.joining("-"));
        System.out.println(s);
    }

    private static void peek(){
        int sum = Stream.of("state","of","the","Lambda", "Libraries", "Edition").peek(System.out::println)
                .map(String::length)
                .filter(length -> length > 4)
                .peek(System.out::println)
                .reduce(0, Integer::sum);

    }

    private static void terminalConsumesString(){
        Stream<Integer> ints = IntStream.range(0,10).boxed();
        ints.forEach(System.out::println);
     //   int sum = ints.reduce(0, Integer::sum);

        IntStream.range(0,10).forEach(System.out::println);
    }

    private static void collectors() {
        System.out.println(Stream.of(1,2,3).toList().get(1));
        System.out.println(Stream.of(1,2,3).collect(Collectors.toList()));
        System.out.println(Stream.of(1,2,3).collect(Collectors.toSet()));

        // other variant
        List<Integer> list2 = Stream.of(1, 2, 3).collect(Collectors.toCollection(ArrayList::new)); // supplier
        Set<Integer> list3 = Stream.of(1, 2, 3).collect(Collectors.toCollection(TreeSet::new)); // sorted
    }

    public static void toMap(){
        Map<Integer,String> stringMap =
                Stream.of("streams","are","awsesome").collect(
                        Collectors.toMap(String::length, s -> s));
        stringMap.forEach((key, value) -> System.out.println(key + ":" + value));

        Map<Integer,String> stringMap2 =
                Stream.of("streams","are","soo","awsesome").collect(
                        Collectors.toMap(String::length, s -> s,
                        (old,n) -> old+","+n));
        stringMap2.forEach((key, value) -> System.out.println(key + ":" + value));
    }

    public static void groupingBy(){
        Map<Integer,List<String>> stringMap2
                =  Stream.of("streams","are","soo","awsesome").collect(
                        Collectors.groupingBy(String::length));

        Map<Integer,List<String>> stringMap3
                =  Stream.of("streams","are","soo","awsesome").collect(
                Collectors.toMap(s -> s.length(),
                        (s) -> { ArrayList<String> tmp = new ArrayList<>();
                            tmp.add(s);
                            return tmp; },
                            (old,n) -> {old.addAll(n); return old; }
                        ));

        stringMap3.forEach((key, value) -> System.out.println(key + ":" + value));
        stringMap2.forEach((key, value) -> System.out.println(key + ":" + value));
    }

    private static void reduce(){
        int sum = IntStream.range(1,10).reduce(0, (a,b) -> a + b); // or Interger::sum

        String[] strings = {"hi", "streams", "are", "awsome"};
        String concat = Arrays.stream(strings).reduce("", (a,b) -> a + b);
        // better
        concat = Arrays.stream(strings).collect(Collectors.joining(" "));
        // even better
        System.out.println(concat);
        concat = String.join(",", strings);
        System.out.println(concat);

        int product = IntStream.of(2, 3, 4, 5)
                .reduce(1, (result, factor) -> result * factor);
        System.out.println(product);//120
    }

    private static void collect(){

        // collect adds to a collection, reduce applies operation
        List<Integer> list1 = Stream.of(1,2,3)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);


        System.out.println(list1);

        StringBuilder sb =  Stream.of("streams","are","awsesome")
                .collect(StringBuilder::new,
                        StringBuilder::append,
                        StringBuilder::append);
        System.out.println(sb);
        String concatenated = Stream.of("streams","are","awsesome")
                .reduce("", (s1,s2) -> s1 + s2); // works but creates new string all the time BAD!
        System.out.println(concatenated);

    }


}
