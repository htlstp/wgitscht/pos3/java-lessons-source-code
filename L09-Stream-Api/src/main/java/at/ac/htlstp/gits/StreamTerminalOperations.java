package at.ac.htlstp.gits;

import java.util.Comparator;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTerminalOperations {

    public static void main(String[] args) {
        count();
        findFirst();
        findAny();
        existence();
        minMax();
    }

    public static void count(){
        System.out.println(Stream.of(1,2,3,4).count());
    }

    public static void findFirst(){
        Stream.of('a','b','c').findFirst().ifPresent(System.out::println);
    }
    public static void findAny(){
        Stream.of('a','b','c').findAny().ifPresent(System.out::println);
    }

    public static void minMax(){
        generateEvenNums(5).max(Comparator.naturalOrder()).ifPresent(System.out::println);
        generateEvenNums(5).min(Comparator.naturalOrder()).ifPresent(System.out::println);
    }

    public static void existence(){
        System.out.println(generateEvenNums(6).allMatch(p -> p % 2 == 0));
        System.out.println(generateEvenNums(6).anyMatch(p -> p == 2));
        System.out.println(generateEvenNums(6).noneMatch(p -> p == 2));

    }

    private static Stream<Integer> generateEvenNums(int number) {
        IntStream.iterate(0, (i) -> i + 2).limit(number);

        return Stream.iterate(0, i -> i + 2).limit(number);
    }

}
