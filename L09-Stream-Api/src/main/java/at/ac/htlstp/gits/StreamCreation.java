package at.ac.htlstp.gits;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLOutput;
import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamCreation {

    public static void main(String[] args) throws IOException, URISyntaxException {
        bufferedReaderLines();
        spacer();
        collectionsStream();
        spacer();
        arrays();
        spacer();
        pattern();
        spacer();
        streamOf();
        spacer();
        fibs();
        spacer();
        iterator();
    }

    private static void iterator() {
        Stream<Integer> myStream = Stream.iterate(1, a -> a+a).limit(10);
        myStream.forEach(System.out::println);
    }

    private static void fibs() {
        class FibSupplier implements Supplier<BigInteger> { // example that lambda not always cuts it
            private final Queue<BigInteger> fibs =
                    new LinkedList<>(Arrays.asList(BigInteger.ZERO, BigInteger.ONE));

            @Override
            public BigInteger get() {
                fibs.offer(fibs.remove().add(fibs.peek()));
                return fibs.peek();
            }
        }
        Stream.generate(new FibSupplier()).limit(10).forEach(System.out::println);
    }

    private static void streamOf() {
        IntStream intStream = IntStream.of(-4, 1, -2, 3);
        Stream<Integer> anotherIntStream = Stream.of(-4, 1, -2, 3);
        anotherIntStream.forEach(System.out::println);
        anotherIntStream.forEach(System.out::println);

        // Supplier e.g. random
        Random random = new Random();
        IntStream randoms = IntStream.generate(random::nextInt).limit(4);
    }

    private static void pattern() {
        final String someString = "a;weired,csv string";
        Pattern.compile("[;, ]")
                .splitAsStream(someString)
                .forEach(System.out::println);
    }


    private static void arrays() {
        Integer[] integers = {1, 2, 3, 4};
        Arrays.stream(integers).forEach(System.out::println);
    }

    private static void spacer() {
        System.out.println("-".repeat(20));
    }

    private static void collectionsStream() {
        Collection<String> someCollection = Arrays.asList("hi", "how", "are", "you", "?");
        someCollection.stream().distinct().forEach(System.out::println);
    }

    private static void bufferedReaderLines() throws URISyntaxException, IOException {
        // Stream from Buffered reader
        URI uri = Objects.requireNonNull(StreamCreation.class.getResource("/lines.csv")).toURI();

        String[] lines;
        try (Stream<String> ls = Files.lines(Path.of(uri))) {
            lines = ls.toArray(String[]::new);
        }
        System.out.println(Arrays.toString(lines)); // lines is ready here
    }


}
