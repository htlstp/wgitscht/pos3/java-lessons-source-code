package at.ac.htlstp.gits.simplify;

import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;

public record Person(String firstname, String name, int age, Address address) {

    public Person { // this only works if we don't override the created construtor
        Objects.requireNonNull(firstname,"need a firstname");
        Objects.requireNonNull(name,"need a name");
    }

    // additional constructor
    public Person(String firstname, String name, int age){
        this(firstname,name,age,null);
    }

    // static Methode
    public static Person of(String csv){
        String[] parts = csv.split(",");
        if(parts.length != 5 || !areNumeric(parts[2], parts[4])) {
            throw new IllegalArgumentException("illegal csv string "+ csv);
        }
        return  new Person(parts[0], parts[1], Integer.parseInt(parts[2]),
                new Address(parts[3], Integer.parseInt(parts[4])));
    }

    private static boolean areNumeric(String ... values){
        return Arrays.stream(values).allMatch(numeric.asMatchPredicate());
    }
    private static final Pattern numeric = Pattern.compile("\\d+");
}
