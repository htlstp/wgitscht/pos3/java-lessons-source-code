package at.ac.htlstp.gits;

import java.util.Objects;
import java.util.Optional;

public class OptionalExample {

    public static void main(String[] args) {
        simple();
        System.out.println("=".repeat(20));
        new OptionalExample().partner();
    }

    private void partner() {
        Person homer = new Person();
        Person marge = new Person();

        Optional<Double> value = Optional.of(3d);
        System.out.println(homer.getPartner().isPresent());
        homer.setPartner(marge);
        System.out.println(homer.getPartner().isPresent());
        homer.getPartner().ifPresent(System.out::println);
    }

    private static void simple() {
        Optional<String> emptyOptional = Optional.empty();
        System.out.println(emptyOptional.isPresent());

        Optional<Integer> integerOptional = Optional.of(3);
        System.out.println(integerOptional.isPresent() + " " + integerOptional.get());
    }


    public class Person {
        private Person partner; // can be null internally..
        public void setPartner(Person partner) {

            this.partner = Objects.requireNonNull(partner); // no null allowed
        }
        public void removePartner() {
            partner = null;
        }
        public Optional<Person> getPartner() {
            // avoid to return null here
            return Optional.ofNullable(partner); // ofNullable otherwhise error
        }

        @Override
        public String toString() {
            return "Person{" +
                    "partner=" + partner +
                    '}';
        }
    }
}
