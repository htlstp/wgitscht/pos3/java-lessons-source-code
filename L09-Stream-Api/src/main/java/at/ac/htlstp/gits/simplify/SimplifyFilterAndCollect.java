package at.ac.htlstp.gits.simplify;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class SimplifyFilterAndCollect {


    private List<Person> personList = new ArrayList<>();

    public static void main(String[] args) throws Exception {

        SimplifyFilterAndCollect simplify = new SimplifyFilterAndCollect();
        simplify.run();
        System.out.println("=".repeat(30));
        simplify.runStreamAPI();
    }

    private void run() throws URISyntaxException, IOException {
        Path fullPath = getPathOfRessource("/persons.csv");
        // read the file
        try(BufferedReader reader = Files.newBufferedReader(fullPath)){
            reader.readLine(); // skip first
            String line;
            while((line = reader.readLine()) != null) {
                personList.add(Person.of(line));
            }
        }

        System.out.println(getAdults().size());
        System.out.println(getMaxAge());
        System.out.println(getAvgAge());
        System.out.println(isSomebodyFromCity("New York"));
    }

    private void runStreamAPI() throws URISyntaxException, IOException {
        Path fullPath = getPathOfRessource("/persons.csv");

        try(BufferedReader reader = Files.newBufferedReader(fullPath)){
            personList = reader.lines().skip(1).map(Person::of).toList();
        }

        System.out.println(personList.stream().filter(p -> p.age() >= 18).toList().size());
        System.out.println(personList.stream()
                .map(Person::age)
                .max(Comparator.naturalOrder()).orElse(0));
        System.out.println(personList.stream().mapToDouble(Person::age).average().orElse(Double.NaN));
        System.out.println(personList.stream().anyMatch(p -> p.address().city().equals("New York")));
        getPersonInCityByAge()
                .forEach((key, value) -> {
                    System.out.println(key);
                    value.forEach((k,v) ->
                    {
                        System.out.println("\t" + k);
                        v.forEach(p -> System.out.println("\t"+p));
                    });
                });
        System.out.println(getPersonsPerCity());
    }

    private Path getPathOfRessource(String location) throws URISyntaxException {
        URL resourceUrl = getClass().getResource(location);
        Path fullPath = resourceUrl != null ? Path.of(resourceUrl.toURI()) : null;
        Objects.requireNonNull(fullPath, "path must not be null");
        return fullPath;
    }

    private boolean isSomebodyFromCity(String city) {
        for(var person: personList){
           if(person.address().city().equals(city)) {
               return true;
           }
        }
        return false;
    }



    private Map<String,List<Person>> getPersonInCity(){
        return personList.stream().collect(
                Collectors.groupingBy(p -> p.address().city())
        );
    }


    private Map<String, Integer> getPersonsPerCity() {
        var res = personList.stream().collect(
                Collectors.toMap(s -> s.address().city(), (s) -> 1,
                        Integer::sum));
        return res;
    }

    private Map<String,Map<Integer,List<Person>>> getPersonInCityByAge(){
        return personList.stream().collect(
                Collectors.groupingBy((p) -> p.address().city(), Collectors.groupingBy(Person::age)));
    }

    private List<Person> getAdults(){
        
        var result = new ArrayList<Person>();
        for(var person: personList){
            if(person.age() >= 18) {
                result.add(person);
            }
        }
        return result;
    }

    private int getMaxAge(){
        var max = 0;
        for(var person: personList){
            if(person.age() > max) {
                max = person.age();
            }
        }
        return max;
    }

    private double getAvgAge(){
        var age = 0d;
        for(var person: personList){
           age += person.age();
        }
        return age/personList.size();
    }


    // just internal stuff to read a yaml file...
    private static List<Person> getPersons(String filename) throws IOException {
        // configure the jackson object mapper
        ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        CollectionType listType = objectMapper.getTypeFactory()
                .constructCollectionType(ArrayList.class, Person.class);

        // read from file by using the object mapper.
        File file = new File(Objects.requireNonNull(SimplifyFilterAndCollect.class.getResource("/"+filename)).getFile());
        return objectMapper.readValue(file, listType);
    }

}
