package at.ac.htlstp.gits;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.stream.Stream;

public class StreamParallelSequential {

    public static void main(String[] args) {

        // order
        order();
        // Peformance
        performance();
    }

    private static void order() {
        Character[] chars = {'a','b','c','d','e'};
        Arrays.asList(chars).parallelStream().forEach(System.out::print);
        System.out.println();
        Arrays.asList(chars).parallelStream().forEachOrdered(System.out::print);
        System.out.println();
        // same
        //Arrays.asList(chars).parallelStream().sequential().forEach(System.out::print);
        System.out.println();
        // order is order in Stream no sort
        Character[] charsOther = {'b','d','c','e','a'};
        Arrays.asList(charsOther).parallelStream().sorted().forEachOrdered(System.out::print);
        System.out.println();
    }

    private static void performance() {
        List<Integer> ints = createStream();
        int value = ints.get(6578697);
        measure(StreamParallelSequential::sumSequential,value,ints);
        measure(StreamParallelSequential::sumParallel,value,ints);
        // no advantage
        measure(StreamParallelSequential::findFirstSequential,value,ints);
        measure(StreamParallelSequential::findFirstParallel,value,ints);
    }

    private static Integer findFirstSequential(int value, List<Integer> ints) {
        return ints.stream().filter(i -> i == value).findFirst().orElse(-1);
    }

    private static Integer findFirstParallel(int value, List<Integer> ints) {
        return ints.parallelStream().filter(i -> i == value).findFirst().orElse(-1);
    }


    private static Integer sumSequential(int value, List<Integer> ints) {
        return ints.stream().reduce(0, Integer::sum);
    }

    private static Integer sumParallel(int value, List<Integer> ints) {
        return ints.parallelStream().reduce(0, Integer::sum);
    }

    private static<T,U,R>  R measure(BiFunction<T,U,R> function, T value, U value2){
        R result;
        Instant start = Instant.now();
        result = function.apply(value, value2);
        Instant end = Instant.now();
        System.out.println("took: " + Duration.between(start,end).toMillis());
        return result;
    }

    private static List<Integer> createStream() {
        Random rand = new Random();
        final int size = (int)Math.pow(10,8);
        return Stream.generate(() -> rand.nextInt(0,size)).limit(size * 2L).toList();
    }
}
