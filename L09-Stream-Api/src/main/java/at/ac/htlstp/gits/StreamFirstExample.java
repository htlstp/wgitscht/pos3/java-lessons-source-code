package at.ac.htlstp.gits;

import java.util.*;

public class StreamFirstExample {
    public static void main(String[] args) {
        firstExample();
    }

    public static void firstExample() {
        System.out.println("First Example");
        Object[] objects = {" ", '3', null, "2", 1, ""};
        Arrays.stream(objects)// creating a stream of an array
                .filter(Objects::nonNull)// do not add null values
                .map(Objects::toString) // map with a Function (in this case a Method reference)
                .map(String::trim) // do as many mapping steps as you like
                .filter(s -> !s.isBlank()) // filter out some values
                .map(Integer::parseInt) // another mapping step
                .filter(i -> i > 1) // filter again (now we have integer)
                .sorted(Comparator.reverseOrder()) // sort (natural oder reversed)
                .forEach(System.out::println);
        System.out.println("=".repeat(20));
        System.out.println("\n".repeat(5));
    }


}