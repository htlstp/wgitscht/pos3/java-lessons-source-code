package at.ac.htlstp.gits;

import java.util.Collections;
import java.util.Objects;

public record MyTest(String foo, int bar) {
    public MyTest {
        Objects.requireNonNull(foo, "fooBar");
        Collections.emptyList();
    }
}
