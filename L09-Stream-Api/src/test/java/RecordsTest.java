import at.ac.htlstp.gits.simplify.Address;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RecordsTest {

    @Test
    void test_records_generated_equals() {
        Address a = new Address("Vienna", 1010);
        Address a2 = new Address("Vienna", 1010);
        assertEquals(a, a2);
    }

    @Test
    void test_records_generated_toString() {
        Address a = new Address("Vienna", 1010);
        assertTrue(a.toString().contains("Vienna") && a.toString().contains("1010"));
    }

    @Test
    void test_records_change_only_by_creation() {
        Address a = new Address("Vienna", 1010);
        Address b = a.withCity("Linz");
        assertTrue(b.toString().contains("Linz"));
    }
}

