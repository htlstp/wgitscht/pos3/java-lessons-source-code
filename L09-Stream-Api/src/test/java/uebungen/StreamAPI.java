package uebungen;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.*;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StreamAPI {


    @Test
    public void oneTo20() {

        // 1
        IntStream.iterate(1, (i) -> i + 1);

        // 2
        IntStream.generate(new IntSupplier() {
            int last = 1;

            @Override
            public int getAsInt() {
                return last++;
            }
        });

        // 3
        Stream.generate(new Supplier<Integer>() {
            int last = 1;

            public Integer get() {
                return last++;
            }
        });
        // 4
        IntStream.range(1, 21).filter((i) -> i % 2 == 1)
                .map((i) -> i * i).forEach(System.out::println);

    }


    @Test
    public void sumSum() {
        System.out.println(IntStream.range(1, 101)
                .mapToDouble((i) -> 1.0 / ((i + 1) * (i + 2))).sum());
    }


    @Test
    public void generateLotto() {
        IntStream.generate(
                () -> new Random().nextInt(1, 46))
                .distinct()
                .limit(6).sorted().forEach((e) -> System.out.print(e + " "));

//                .boxed().map(Objects::toString)
//                .collect(Collectors.joining(" ")));
    }


    @Test
    public void intArray() {
        int[] array = {8, 6, 1, 8, 3, 9, 7, 3, 5, 13, 8, 1, 4, 5};
        Arrays.stream(array).filter((i) -> i % 2 == 1).distinct().map(i -> i * i)
                .sorted().forEach(System.out::println);
    }

    @Test
    public void faktorielle() {

        System.out.println(LongStream.range(1, 21).reduce(1, (a, b) -> a * b));
    }


    @Test
    public void findNumbersWithEvenDigits(){
        var res = IntStream.range(1,101).reduce(0,(a,b) -> {
            return Objects.toString(b).length() > 1 ? a + 1 : a;
        });
        System.out.println(res);
    }



    @Test
    public void sumUp(){
        var n = 6;
        var res = IntStream.range(1,100)
                .reduce(0,(a,b) -> a + b);
        System.out.println(res);
    }

    @Test
    public void numberString(){
        var n = IntStream.range(1,10).collect(
                () -> new StringBuilder(),
                (r,s) -> r.append(s),
                (a,b) -> a.append(b)
        );
        System.out.println(n);
    }


    @Test
    public void zaehleEinser() {
        long num = IntStream.range(1, 1001)
                .mapToObj(Objects::toString)
                .collect(Collectors.joining())
                .chars().filter((c) -> c == '1').count();
        System.out.println(num);
    }

    @Test
    public void faktLaenge() {
        var res = Stream.generate(new Supplier<BigInteger>() {
            private int i = 0;
            private BigInteger faktorielle = BigInteger.ONE;

            @Override
            public BigInteger get() {
                faktorielle = faktorielle.multiply(BigInteger.valueOf(++i));
                return faktorielle;
            }
        }).map(Objects::toString).filter((s) -> s.length() > 99).findFirst();
        System.out.println(res);
    }

    @Test
    public void faktN() {

        BigInteger searchValue = BigInteger.valueOf(10).pow(10000);
        var res = Stream.generate(new Supplier<AbstractMap.SimpleEntry<Integer,BigInteger>>() {
            private int i = 0;
            private BigInteger faktorielle = BigInteger.ONE;

            @Override
            public AbstractMap.SimpleEntry<Integer,BigInteger> get() {
                faktorielle = faktorielle.multiply(BigInteger.valueOf(++i));
                return new AbstractMap.SimpleEntry<>(i, faktorielle);
            }
        }).filter((i) -> i.getValue().compareTo(searchValue) >= 0)
                .findFirst().map(AbstractMap.SimpleEntry::getKey);
        System.out.println(res);
    }



}

