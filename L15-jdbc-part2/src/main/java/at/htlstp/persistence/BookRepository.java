package at.htlstp.persistence;

import at.htlstp.domain.Book;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface BookRepository {
    Optional<Book> findById(int id) throws SQLException;

    Book save(Book book) throws SQLException;
    List<Book> findBooksByPublishYear(Integer year); // can pass null
}
