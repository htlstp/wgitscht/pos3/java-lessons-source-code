package at.htlstp.persistence;

import at.htlstp.domain.Author;

import java.sql.*;
import java.util.List;

public record JdbcAuthorRepository(Connection connection)  implements AuthorRepository {

    @Override
    public List<Author> findAll() throws SQLException {
        return null;
    }

    @Override
    public Author save(Author author) throws SQLException {
        String sql = """
                INSERT INTO authors (name, country) 
                VALUES (?, ?)
            """;
        try (PreparedStatement stmt = connection.prepareStatement(sql,
                Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, author.getName());
            stmt.setString(2, author.getCountry());

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0) throw new SQLException("failed to save author");
            ResultSet rs = stmt.getGeneratedKeys();
            if (!rs.next()) throw new SQLException("failed to save author");
                author.setId(rs.getInt(1));

        }
        return author;
    }

}
