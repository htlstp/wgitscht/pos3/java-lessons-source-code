package at.htlstp.persistence;

import at.htlstp.domain.Author;

import java.sql.SQLException;
import java.util.List;

public interface AuthorRepository {

    List<Author> findAll() throws SQLException;

    Author save(Author author) throws SQLException;
}
