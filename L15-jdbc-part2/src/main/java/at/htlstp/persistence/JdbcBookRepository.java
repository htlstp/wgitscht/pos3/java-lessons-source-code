package at.htlstp.persistence;

import at.htlstp.domain.Author;
import at.htlstp.domain.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public record JdbcBookRepository(Connection connection) implements BookRepository {

    @Override
    public Optional<Book> findById(int bookId) throws SQLException {
        String sql = """
                SELECT books.*, authors.* FROM books
                INNER JOIN authors ON books.author_id = authors.author_id
                WHERE books.book_id = ?
                """;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, bookId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                Author author = new Author(rs.getInt("author_id"),
                        rs.getString("name"), rs.getString("country"));
                return Optional.of(new Book(rs.getInt("book_id"),
                        rs.getString("title"), rs.getInt("published_year"), author));
            }
        }
        return Optional.empty();
    }

    public Book save(Book book) throws SQLException {
        if (book.getId() != null) {
            throw new IllegalArgumentException("can't save exisiting");
        }
        String sql = """
                INSERT INTO books (author_id, title, published_year) 
                VALUES (?, ?, ?)
                """;
        try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setInt(1, book.getAuthor().getId());
            stmt.setString(2, book.getTitle());
            stmt.setInt(3, book.getPublishedYear());

            int affectedRows = stmt.executeUpdate();
            if (affectedRows > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        book.setId(rs.getInt(1)); // Retrieve and set the generated book ID
                    } else {
                        throw new SQLException("Saving book failed.");
                    }
                }
            }
        }
        return book;
    }

    @Override
    public List<Book> findBooksByPublishYear(Integer year) {
        List<Book> books = new ArrayList<>();
        String sql = """
                SELECT * FROM books 
                WHERE books.published_year
                """ + (year == null ? "IS NULL" : "= ?");

        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            if (year != null)
                stmt.setInt(1, year);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {

                Integer publishedYear = rs.getInt("published_year");
                if(rs.wasNull())
                    publishedYear = null;

                books.add(new Book(rs.getInt("book_id"),
                        rs.getString("title"),
                        publishedYear,
                        null));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }
}
