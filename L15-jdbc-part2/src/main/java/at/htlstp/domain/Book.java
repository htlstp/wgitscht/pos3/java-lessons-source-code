package at.htlstp.domain;

public class Book {

    private Integer id;

    private String title;
    private Integer publishedYear;
    private Author author;

    public Book(Integer id, String title, Integer publishedYear, Author author) {
        this.id = id;
        this.title = title;
        this.publishedYear = publishedYear;
        this.author = author;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(Integer publishedYear) {
        this.publishedYear = publishedYear;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
