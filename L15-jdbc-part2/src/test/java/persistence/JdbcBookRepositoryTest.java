package persistence;

import at.htlstp.domain.Author;
import at.htlstp.domain.Book;
import at.htlstp.persistence.BookRepository;
import at.htlstp.persistence.JdbcBookRepository;
import org.junit.jupiter.api.*;
import persistence.setup.TestConnectionSupplier;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

class JdbcBookRepositoryTest {

    private  Connection connection;
    private BookRepository bookRepository;

    @BeforeEach
    void createRepository() throws SQLException {
        connection = TestConnectionSupplier.getConnection();
        bookRepository = new JdbcBookRepository(connection);
    }

    @AfterEach
    void closeConnection() throws SQLException {
        connection.close();
    }

    @Test
    void testFindById() throws SQLException {
        connection.createStatement().executeUpdate("INSERT INTO authors (author_id, name, country) VALUES (1, 'Test Author', 'Test Country')");
        connection.createStatement().executeUpdate("INSERT INTO books (book_id, title, published_year, author_id) VALUES (1, 'Test Book', 2021, 1)");

        Optional<Book> foundBook = bookRepository.findById(1);
        Assertions.assertTrue(foundBook.isPresent());
        Assertions.assertEquals("Test Book", foundBook.get().getTitle());
    }

    @Test
    void testSaveNewBook() throws SQLException {
        Author author = new Author(1, "New Author", "New Country");
        connection.createStatement().executeUpdate("INSERT INTO authors (author_id, name, country) VALUES (1, 'New Author', 'New Country')");
        Book newBook = new Book(null, "New Book", 2021, author);

        Book savedBook = bookRepository.save(newBook);
        Assertions.assertNotNull(savedBook.getId());

        Optional<Book> retrievedBook = bookRepository.findById(savedBook.getId());
        Assertions.assertTrue(retrievedBook.isPresent());
        Assertions.assertEquals("New Book", retrievedBook.get().getTitle());
    }

    @Test
    void testFindBooksByPublishYear() throws SQLException {
        connection.createStatement().executeUpdate("INSERT INTO authors (author_id, name, country) VALUES (1, 'Author One', 'Country')");
        connection.createStatement().executeUpdate("INSERT INTO books (booK_id, title, published_year, author_id) VALUES (1, 'Book One', NULL, 1)");
        connection.createStatement().executeUpdate("INSERT INTO books (booK_id, title, published_year, author_id) VALUES (2, 'Book Two', 2021, 1)");

        List<Book> booksWithoutYear = bookRepository.findBooksByPublishYear(null);
        Assertions.assertEquals(1, booksWithoutYear.size());
        Assertions.assertNull(booksWithoutYear.get(0).getPublishedYear());

        List<Book> books2021 = bookRepository.findBooksByPublishYear(2021);
        Assertions.assertEquals(1, books2021.size());
        Assertions.assertEquals(Integer.valueOf(2021), books2021.get(0).getPublishedYear());
    }
}
