CREATE TABLE authors
(
    author_id IDENTITY NOT NULL PRIMARY KEY,
    name    VARCHAR(100) NOT NULL,
    country VARCHAR(50)
);

CREATE TABLE books
(
    book_id IDENTITY NOT NULL PRIMARY KEY,
    author_id      INT NOT NULL,
    title          VARCHAR(255),
    published_year INT,
    FOREIGN KEY (author_id) REFERENCES authors (author_id) ON DELETE CASCADE
);