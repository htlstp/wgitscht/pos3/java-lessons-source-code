package counter;

import javafx.beans.Observable;
import javafx.beans.property.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class FxCounterController implements Initializable {
    @FXML
    private Label counterText;
    @FXML
    private Button decreaseButton;
    @FXML
    private Button increaseButton;

    private IntegerProperty counter;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        counter = new SimpleIntegerProperty(0);
        // listener with lambda
        counter.addListener(this::listener);

        // eventhandler
        decreaseButton.setOnMouseClicked(this::decreaseClick);
        increaseButton.setOnMouseClicked(this::increaseClick);
        // set field initial value
        counterText.setText(String.valueOf(counter.getValue()));

    }

    private void listener(Observable observable) {
        counterText.setText(String.valueOf(counter.getValue()));
    }

    private void decreaseClick(MouseEvent mouseEvent) {;
        counter.set(counter.get() - 1);
    }

    private void increaseClick(MouseEvent mouseEvent) {
        counter.set(counter.get() + 1);
    }

    @FXML
    private void onIncreaseButton(ActionEvent actionEvent) {
    }

    public void onDecreaseButton(ActionEvent actionEvent) {
    }
}
