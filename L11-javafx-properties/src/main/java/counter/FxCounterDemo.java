package counter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class FxCounterDemo extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        URL file = this.getClass().getResource("/layout/counter-v2.fxml");
        Parent parent = FXMLLoader.load(file);
        primaryStage.setScene(new Scene(parent));
        primaryStage.show();
    }
}
