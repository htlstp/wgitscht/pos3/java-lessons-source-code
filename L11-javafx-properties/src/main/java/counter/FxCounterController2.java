package counter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class FxCounterController2 implements Initializable {
    @FXML
    private Label counterText;

    private Counter counter;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        counter = new Counter(-1);
        counter.valueProperty().addListener(
                l -> counterText.setText(Integer.toString(counter.getValue())));
        counter.reset();
    }

    @FXML
    private void onIncreaseButton(ActionEvent actionEvent) {
        counter.inc();
    }

    @FXML
    private void onDecreaseButton(ActionEvent actionEvent) {
        counter.dec();
    }

    @FXML
    private void onClearButton(ActionEvent actionEvent) {
        counter.reset();
    }
}
