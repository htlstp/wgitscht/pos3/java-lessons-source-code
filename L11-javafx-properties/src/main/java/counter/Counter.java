package counter;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Created: 13.03.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class Counter {
    private IntegerProperty value = new SimpleIntegerProperty(this, "value", 0);

    public Counter(int value) {
        this.value.set(value);
    }

    public IntegerProperty valueProperty(){
        return value;
    }

    public final int getValue(){
        return this.value.get();
    }

    public final void setValue(int i){
        this.value.set(i);
    }

    public final void inc(){
        this.value.set(this.value.get()+ 1);
    }

    public final void dec(){
        if(this.value.get() > 0) {
            this.value.set(this.value.get() - 1);
        }
    }

    public final void reset(){
        this.value.set(0);
    }
}
