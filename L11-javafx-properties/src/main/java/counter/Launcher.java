package counter;

import counter.FxCounterDemo;
import javafx.application.Application;

public class Launcher {
    public static void main(String[] args) {
        Application.launch(FxCounterDemo.class);
    }
}
