package observer;

/**
 * Created: 13.03.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class ObserverTest {
    public static void main(String[] args) {
        ObservableCounter counter = new ObservableCounter();
        counter.addObserver(() -> System.out.println("lambda observer!"));
        counter.addObserver(new CounterObserver("obs1",counter));
        counter.addObserver(new CounterObserver("obs2",counter));
        CounterObserver obs3 = new CounterObserver("obs3", counter);
        counter.addObserver(obs3);
        counter.inc();
        counter.removeObserver(obs3);
        counter.inc();
    }
}


class CounterObserver implements Observer {

    private final ObservableCounter counter;
    private final String name;


    CounterObserver(String name,ObservableCounter counter){
        this.counter = counter;
        this.name = name;
    }

    @Override
    public void refresh() {
        System.out.println("counterobserver "+ name +" observed:" + counter.getValue());
    }
}