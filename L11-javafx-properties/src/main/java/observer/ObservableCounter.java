package observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created: 13.03.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class ObservableCounter {

    private int value;
    private List<Observer> observers;

    public ObservableCounter(){
        value = 0;
        observers = new ArrayList<>();
    }

    public void inc(){
        value++;
        doNotify();
    }

    public void addObserver(Observer o){
        observers.add(o);
    }

    public void removeObserver(Observer o){
        observers.remove(o);
    }

    public int getValue(){
        return value;
    }

    private void doNotify(){
        observers.forEach(Observer::refresh);
    }
}
