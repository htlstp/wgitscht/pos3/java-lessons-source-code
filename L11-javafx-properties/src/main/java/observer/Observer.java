package observer;

/**
 * Created: 13.03.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public interface Observer {
    void refresh();
}
