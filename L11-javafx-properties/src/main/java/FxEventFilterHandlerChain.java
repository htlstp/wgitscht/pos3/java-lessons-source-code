import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Created: 13.03.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class FxEventFilterHandlerChain extends Application {

    private static final int RADIUS = 50;

    private final CheckBox consumeEventCbx = new CheckBox("Consume Mouse Click at Circle");
    private final TextArea log = new TextArea();

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox();
        root.setPrefSize(800, 600);
        Circle circle = new Circle(RADIUS, Color.DODGERBLUE);
        Circle circle2 = new Circle(RADIUS, Color.CORAL);


        // ------------- circle event handler and filter
        circle.addEventFilter(MouseEvent.MOUSE_CLICKED, e -> append("CIRCLE Event Filter triggered"));
        circle.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            append("CIRCLE Event Handler triggered");

            if (consumeEventCbx.isSelected()) {
                e.consume(); // no other handlers  are called (bubbling phase) (filters are called already!)
            }
        });

        // -------------- pane event handler
        root.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            append("PANE Event Handler triggered");
        });

        root.addEventFilter(MouseEvent.MOUSE_CLICKED, e -> {
            append("PANE Event Filter triggered");
            //  e.consume(); // no other handlers/filters are called (capture phase)
        });

        root.getChildren().addAll(circle, circle2, consumeEventCbx);
        root.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            if (e.getCode() == KeyCode.F1) {
                append("HELP requested :)");
            }
        });
        jsutForDemo(root);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void append(String msg) {
        log.appendText(msg + "\n");
    }

    private void jsutForDemo(Pane root) {
        log.setFont(new Font(22));
        log.setPrefSize(800, 400);
        root.getChildren().add(log);
        Button clear = new Button("clear");
        clear.setOnMouseClicked(e -> log.clear());
        root.getChildren().add(clear);
    }


    private void findWhatCircle(Event e) {
        EventTarget target = e.getTarget();
        if (target instanceof Circle) {
            Circle circleClicked = (Circle) target;
            if (circleClicked.getFill()  == Color.DODGERBLUE) {
                append("blue circled clicked ");
            } else {
                append("red circled clicked ");
            }
        }
    }
}
