/**
 * Created: 13.03.2022
 *
 * @author Werner Gitschthaler (Werner)
 */

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class ChangeTest {
    public static void main(String[] args) {
        IntegerProperty money = new SimpleIntegerProperty(1);
        // invalidation listener (get's only fired once if the value is invalid)
        money.addListener(observable -> System.out.println("invalidated"));
        money.set(10);
        money.set(20);
        money.set(30);
        System.out.println(money.getValue());
        money.set(40);


        IntegerProperty moreMoney = new SimpleIntegerProperty(1);
        moreMoney.addListener((observable, oldValue, newValue) -> System.out.println("changed"));
        moreMoney.set(100);
        moreMoney.set(200);
        moreMoney.set(300);
        System.out.println(moreMoney.getValue());
    }

}
