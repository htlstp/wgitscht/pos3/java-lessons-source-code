package lesson1;

import java.util.stream.IntStream;

public class ThreadPassingParameters {
    public static void main(String[] args) {
        // constructor parameter
        new Thread(new NumberCommandParametized(10)).start();
        var limit = 110;
        // lambda and local variable
        new Thread(() -> IntStream.range(limit,limit+10).forEach(System.out::println)).start();
    }
}

class NumberCommandParametized implements Runnable {
    private int limit;
    public NumberCommandParametized(int limit) {
        this.limit = limit;
    }

    @Override
    public void run() {
        IntStream.range(0,limit).forEach(System.out::println);
    }
}
