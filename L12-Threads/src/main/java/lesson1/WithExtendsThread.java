package lesson1;

import java.time.LocalDateTime;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class WithExtendsThread {
    public static void main(String[] args) {
        System.out.println("-".repeat(20) + "single threaded!");
        new NumberThread(1000).run(); // run does not start a new thread
        new NumberThread(2000).run(); // run does not start a new thread
        System.out.println("-".repeat(20) + "multi threaded!");
        new NumberThread(3000).start();
        new NumberThread(4000).start();
        // self starting
        new SelfStarter();

    }
}

class SelfStarter extends Thread {
    // rarely used
    public SelfStarter(){
        start();
    }

    public void run() {
        Stream.generate(LocalDateTime::now).limit(5).forEach(System.out::println);
    }
}

class NumberThread extends Thread {
    private final int range;

    public NumberThread(int range) {
        this.range = range;
    }

    @Override
    public void run(){
        IntStream.range(range, range+ 10).forEach(System.out::println);
    }
}
