package lesson1;

import java.io.BufferedReader;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ThreadStates {


    public static void main(String[] args) throws InterruptedException {

        Runnable canBeInterrupted = () -> {
            var numberOfInterrupts = 0;
            while(!Thread.interrupted()) {
                System.out.println("I am running..");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                        Thread.currentThread().interrupt(); // interupt again since we catched it
                }
            }
        };
        Thread t1 = new Thread(canBeInterrupted);
        t1.start();
        Thread.sleep(2000);
        t1.interrupt();
       // Thread.sleep(2000);
       // Scanner s = new Scanner(System.in);
      //  s.nextLine();
        //t1.interrupt();
    }


}
