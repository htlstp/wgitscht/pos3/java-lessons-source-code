package lesson1;

import java.util.function.Predicate;

public class ThreadString {

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();

        Thread t1 = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                sb.append("A");
            }
        });
        Thread t2 = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                sb.append("B");
            }
        });

        Thread t3 = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted())
                synchronized (sb) {
                    var found = sb.toString().chars().
                            mapToObj(Integer::valueOf).anyMatch(new Predicate<>() {
                                int last = 0;

                                @Override
                                public boolean test(Integer integer) {
                                    return last == integer;
                                }
                            });
                    if(found) {
                        System.out.println("found");
                    }
                }
            });

        t1.start();
        t2.start();
        t3.start();
    }


}
