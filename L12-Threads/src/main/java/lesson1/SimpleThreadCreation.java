package lesson1;

import java.time.LocalDateTime;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SimpleThreadCreation {
    public static void main(String[] args) {
        Thread printTimeThread = new Thread(new TimeCommand());
        Thread printNumberThread = new Thread(new NumberCommand());
        printTimeThread.start();
        printNumberThread.start();
        // start thrread without reference
        new Thread(new NumberCommand()).start();
    }
}

class NumberCommand implements Runnable {

    @Override
    public void run() {
        IntStream.range(0,20).forEach(System.out::println);
    }
}

class TimeCommand implements Runnable {

    @Override
    public void run() {
        Stream.generate(LocalDateTime::now).limit(20)
                .forEach(System.out::println);
    }
}