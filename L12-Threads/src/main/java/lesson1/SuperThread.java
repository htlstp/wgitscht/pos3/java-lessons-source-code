package lesson1;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class SuperThread {

    public static void main(String[] args) {
        Thread t1 = new MyThread(9);
        t1.setName("eins");
        Thread t2 = new MyThread(2);
        t2.setName("zwei");
        Thread t3 = new MyThread(3);
        t3.setName("drei");
        System.out.println("start");
        t1.start();
        t2.start();
        t3.start();
        System.out.println("end");
    }
}


class MyThread extends Thread {


    long sleepTime = 2;

    public MyThread(long howLong) {
        if (howLong < 0 || howLong > 10)
            this.sleepTime = 0;
        else
            this.sleepTime = howLong;
    }

    @Override
    public void run() {
        System.out.println("Started " + Thread.currentThread().getName());
        try {
            TimeUnit.SECONDS.sleep(this.sleepTime);
        } catch (InterruptedException e) {
            System.out.println("interrupted");
            throw new RuntimeException(e);
        }
        IntStream.range(0, 3).forEach(s ->
                System.out.println(Thread.currentThread().getName() + " " + s));
    }
}
