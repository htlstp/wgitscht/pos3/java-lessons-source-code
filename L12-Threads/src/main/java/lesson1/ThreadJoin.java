package lesson1;

import java.time.LocalDateTime;

public class ThreadJoin {
    public static void main(String[] args) throws InterruptedException {
        DateRunner d1 = new DateRunner();
        Thread t1 = new Thread(d1);
        t1.start();
        t1.join();
        System.out.println(d1.getResult());
    }
}

class DateRunner implements Runnable{
    private LocalDateTime result;

    public LocalDateTime getResult() {
        return result;
    }

    public void run(){
        this.result = LocalDateTime.now();
    }
}