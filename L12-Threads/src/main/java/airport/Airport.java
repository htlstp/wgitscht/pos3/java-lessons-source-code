package airport;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Airport {


    Lock lockE = new ReentrantLock();

    public void land(Airstrip airstrip, Plane p) {
        airstrip.lock.lock();
        try {
            lockE.lock();
            try {
                System.out.printf("Plane %s is on %s%n",p.name(), airstrip.name);
                sleep(1);

            } finally {
                System.out.printf("Plane %s passed E%n",p.name());
                lockE.unlock();
            }
            sleep(2);
        } finally {
            System.out.printf("Plane %s left strip %s%n",p.name(), airstrip.name);
            airstrip.lock.unlock();
        }
    }

    public void land(boolean airstrip, Plane p) {
        if(airstrip) land(Airstrip.AB, p);
        else land(Airstrip.CD, p);
    }


    enum Airstrip {
        AB(new ReentrantLock(), "AB"),
        CD(new ReentrantLock(), "CD"),
        ;

        public final ReentrantLock lock;
        public final String name;

        Airstrip(ReentrantLock reentrantLock, String ab) {
            this.lock = reentrantLock;
            this.name = ab;
        }
    }

    void sleep(int sec) {
        try {
            TimeUnit.SECONDS.sleep(sec);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
