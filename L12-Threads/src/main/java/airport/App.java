package airport;

public class App {

    public static void main(String[] args) {
        Airport a = new Airport();
        Plane plane = new Plane("Plane 1", a, true);
        Plane plane1 = new Plane("Plane 2", a, false);
        Plane plane2 = new Plane("Plane 3", a, true);

        plane.start();
        plane1.start();
        plane2.start();
    }
}
