package airport;

import java.util.concurrent.ThreadLocalRandom;

public class Plane extends Thread {

    private final String name;
    private Airport airport;

    private boolean airstrip;

    public Plane(String name,Airport airport, boolean airstrip) {
        this.name = name;
        this.airport = airport;
        this.airstrip = airstrip;
    }

    public String name() {
        return name;
    }


    @Override
    public void run() {
        airport.land(airstrip, this);
    }

}
