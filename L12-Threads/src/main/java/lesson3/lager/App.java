package lesson3.lager;

public class App {

    public static void main(String[] args) {
        Lager lager = new Lager();
        System.out.println("Verkauf startet Bestand ist: " + lager.getBestand());
        new Thread(new Lieferant(lager),"Lieferant").start();
        new Thread(new Kunde(lager),"Kunde").start();
    }
}
