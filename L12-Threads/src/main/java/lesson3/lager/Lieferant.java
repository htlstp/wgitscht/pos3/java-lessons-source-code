package lesson3.lager;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created: 29.05.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class Lieferant extends Thread {

    private final Lager lager;

    public Lieferant(Lager lager){
        this.lager = lager;
    }

    @Override
    public void run() {
        while (lager.istOffen()){
            lager.liefere(ThreadLocalRandom.current().nextInt(1, 11));
            sleepRandom();
        }
    }

    private void sleepRandom() {
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(100,800));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
