package lesson3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class GlobalExceptionHandlerDemo {
        public static void main( String[] args ) throws Exception {
            Thread.setDefaultUncaughtExceptionHandler(GlobalExceptionHandler.INSTANCE);

            // no special handler
            Thread zeroDivisor = new Thread( () -> System.out.println( 1 / 0 ) );
            zeroDivisor.start();

            // specific handler
            Thread indexOutOfBound =  new Thread( () -> System.out.println( (new int[0])[1] ) );
            indexOutOfBound.setUncaughtExceptionHandler( ( t, e ) -> {
                System.out.println("specific: index out of bounds");
            } );
            indexOutOfBound.start();


            ExecutorService service = Executors.newFixedThreadPool(3,
                    (d) -> {
                        Thread thread = new Thread(d);
                        thread.setUncaughtExceptionHandler((e,t) -> {
                            System.out.println("Executor excpetion handler");
                        });
                        return thread;
                    });
            List<Future<Object>> futures = new ArrayList<>();
            for(int i = 0; i < 3; i++) {
                futures.add(service.submit(() -> {
                    System.out.println("happily running ");
                    throw new RuntimeException("Planned exception after execute()");
                }));

               service.execute(() -> {
                    System.out.println("happily running ");
                    throw new RuntimeException("Planned exception after execute()");
                });
            }
            for(Future f : futures){
                try {
                    f.get();
                } catch(Exception e) {
                    System.out.println("result of get was wrapeed in ex "+ e.getMessage());
                }
            }
            service.shutdown();
    }
}


enum GlobalExceptionHandler implements Thread.UncaughtExceptionHandler {
    INSTANCE;
    @Override public void uncaughtException(Thread thread, Throwable uncaughtException ) {
        System.out.println("global: "+uncaughtException.getMessage() +  " from thread " + thread.getName());
    }
}