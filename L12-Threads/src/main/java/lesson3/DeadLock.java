package lesson3;

public class DeadLock {

    public static void main(String[] args) {
        new DeadLock().run();
    }

    private void run() {
        Object lock1 = new Object();
        Object lock2 = new Object();
        Thread t1 = new Thread(new DeadlockThread<>(lock1,lock2));
        Thread t2 = new Thread(new DeadlockThread<>(lock2,lock1));
        t1.start();
        t2.start();
    }
}

class DeadlockThread<T> implements Runnable {

    private final T lock1;
    private final T lock2;


    public DeadlockThread(T lock1, T lock2) {
        this.lock1 = lock1;
        this.lock2 = lock2;
    }

    @Override
    public void run() {

        for(int i = 0; i < 3; i++) {
            var name = Thread.currentThread().getName();
            System.out.println("starting thread " + name);
            synchronized (this.lock1) {
                System.out.println("lock 1 is held by" + name);
                System.out.println("    getting lock2 " + name);
                synchronized (this.lock2) {
                    System.out.println("    lock 2 is held by" + name);
                }
            }
        }
    }
}
