package lesson3.simplerace;

import java.util.List;
import java.util.stream.IntStream;

public class CounterRacingSynced {

    private Counter counter = new Counter(0);

    public static void main(String[] args) throws InterruptedException {
        new CounterRacingSynced().run();
    }

    Runnable r = () -> {
        for(int i=0;i< 2000;i++){
            counter.inc();
        }
    };
    private void run() throws InterruptedException {
        List<Thread> myThreads = IntStream.range(0,20)
                .mapToObj(i -> new Thread(r,"Worker-"+i)).toList();
        myThreads.forEach(Thread::start);
        for(Thread t : myThreads) {
            t.join();
        }
        System.out.println("counter is: "+counter.getCounter());
    }
}

class Counter {
    private int counter;

    public Counter(int counter) {
        this.counter = counter;
    }

    public synchronized int getCounter() {
        return counter;
    }

    public synchronized void inc(){
        counter++;
    }
}