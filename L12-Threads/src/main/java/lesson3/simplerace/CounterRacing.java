package lesson3.simplerace;

import java.util.List;
import java.util.stream.IntStream;

public class CounterRacing {

    private int counter;

    private Runnable r = () -> {
        for(int i=0;i<2000;i++) {
                counter++;
        }
    };
    public static void main(String[] args) throws InterruptedException {
        new CounterRacing().execute();
    }

    private void execute() throws InterruptedException {
        List<Thread> myThreads =
                IntStream.range(0,20).mapToObj(i -> new Thread(r, "Worker-"+i)).toList();
        myThreads.forEach(Thread::start);
        for (Thread myThread : myThreads) {
            myThread.join();
        }
        System.out.println("Counter at the end: " +counter );
    }
}


