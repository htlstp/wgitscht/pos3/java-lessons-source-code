package solutionmini;

import java.util.stream.IntStream;

public class Kindergarten {
    public static void main(String[] args) {
        Paintbox paintbox = new Paintbox(20);
        IntStream.range(0,10).forEach((i) -> {
            var c = new Thread(new Child("Kind" +i, paintbox));
            c.start();
        });
    }
}
