package solutionmini;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

class Child implements Runnable {
    private final String name;
    private final Paintbox paintbox;

    public Child(String name, Paintbox paintbox) {
        this.name = name;
        this.paintbox = paintbox;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            int requiredPens = ThreadLocalRandom.current().nextInt(1, 10 + 1);
            paintbox.acquirePens(requiredPens);
            System.out.printf("%s got %d pens%n", name, requiredPens);
            try {
                TimeUnit.MILLISECONDS.sleep(
                        ThreadLocalRandom.current().nextInt(1000, 3000));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            paintbox.releasePens(requiredPens);
            System.out.printf("%s returned %d pens%n", name, requiredPens);
            try {
                TimeUnit.SECONDS.sleep(
                        ThreadLocalRandom.current().nextInt(1, 5 + 1));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}