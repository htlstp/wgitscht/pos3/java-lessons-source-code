package solutionmini;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Paintbox {
    private int freeNumberOfPens;
    private final Lock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();

    public Paintbox(int maximumNumberOfPens) {
        freeNumberOfPens = maximumNumberOfPens;
        System.out.printf("Paintbox equipped with %s pens%n", freeNumberOfPens);
    }

    public void acquirePens(int numberOfPens) {
        try {
            lock.lock();
            while (freeNumberOfPens < numberOfPens) {
                System.out.printf("%d pens from paintbox requested, available only %d, someone has to wait :(%n",
                        numberOfPens, freeNumberOfPens);
                condition.await();
            }
            freeNumberOfPens -= numberOfPens;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
    }

    public void releasePens(int numberOfPens) {
        try {
            lock.lock();
            freeNumberOfPens += numberOfPens;
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
