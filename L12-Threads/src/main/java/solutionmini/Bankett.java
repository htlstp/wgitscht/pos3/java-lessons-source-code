package solutionmini;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;

public class Bankett {

    public static void main(String[] args) {
        Semaphore seats = new Semaphore(6 - 1);

        class Guest implements Runnable {
            private final String name;

            public Guest(String name) {
                this.name = name;
            }

            @Override
            public void run() {
                try {
                    System.out.printf("%s is waiting for a free place%n", name);
                    seats.acquire();
                    System.out.printf("%s has a seat at the table%n", name);
                    Thread.sleep(ThreadLocalRandom.current().nextInt(2000, 5000));
                } catch (InterruptedException e) { /* Ignore */
                } finally {
                    System.out.printf("%s leaves the table%n", name);
                    seats.release();
                }
            }
        }
        List<String> names = new ArrayList<>(
                Arrays.asList("Chris", "Franz", "Herbert", "Matthias", "Patrick",
                        "Klara", "Eva", "Bernhard", "Martin"));
        ExecutorService executors = Executors.newCachedThreadPool();
        for (String name : names)
            executors.execute(new Guest(name));
        executors.shutdown();
    }
}



