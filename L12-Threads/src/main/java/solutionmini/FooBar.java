package solutionmini;

import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FooBar {


    static Object myLock;
    static Lock fooLock = new ReentrantLock();
    void foo() {
        synchronized (myLock) {

        }
    }

    void bar ( ) {
        fooLock.lock();
        try {
            //todo
        } finally {
             fooLock.unlock();
        }
    }
}
