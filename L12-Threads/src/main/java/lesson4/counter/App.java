package lesson4.counter;

import java.util.concurrent.ThreadLocalRandom;

public class App {
    public static void main(String[] args) {

        CounterWithMinMax counter = new CounterWithMinMax();
        Runnable up = () -> {
            for (int i = 0; i < 20; i++) {
                try {
                    counter.countUp();
                    Thread.sleep(ThreadLocalRandom.current().nextInt(1, 3) * 100);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        Runnable down = () -> {
            for (int i = 0; i < 20; i++) {
                try {
                    counter.countDown();
                    Thread.sleep(ThreadLocalRandom.current().nextInt(1, 3) * 100);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        Thread up1 = new Thread(up);
        Thread up2 = new Thread(up);
        Thread d1 = new Thread(down);
        Thread d2 = new Thread(down);

        up1.start();
        up2.start();
        d1.start();
       // d2.start();
    }
}