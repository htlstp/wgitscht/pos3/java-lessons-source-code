package lesson4.counter;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class CounterWithMinMax {

    ReentrantLock lock = new ReentrantLock();
    Condition tooLow = lock.newCondition();
    Condition tooHigh = lock.newCondition();
    int counter = 0;
    int min = 0;
    int max = 30;

    public void countUp() throws InterruptedException {
        lock.lock();
        try {
            while (counter == max) {
                tooLow.await();
            }
            counter++;
            tooHigh.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public void countDown() throws InterruptedException {
        lock.lock();
        try {
            while (counter == min) {
                tooHigh.await();
            }
            counter--;
            tooLow.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
