package lesson4;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Stack<E> {

    private int size = 0;

    private ReentrantLock lock = new ReentrantLock();
    private Condition stackFull = lock.newCondition();
    private Condition stackEmpty = lock.newCondition();

    private E[] data;
    Stack(int capacity) {
        data = (E[]) new Object[capacity];
    }

    public synchronized void push(E element) throws InterruptedException {
        while(size == data.length) {
            wait();
            System.out.println("Push woke up can work? " + (size < data.length));
        }
        data[size++] = element;
        notify();
    }

    public synchronized E pop() throws InterruptedException {
        while(size == 0) {
            wait();
            System.out.println("Pop woke up can work? " + (size != 0));
        }
        var ele = data[--size];
        notify();
        return ele;
    }

    public E popLock() throws InterruptedException {
        lock.lock();
        try {
            while (size == 0) {
                stackEmpty.await();
                System.out.println("Pop woke up can work? " + (size != 0));

            }
            var ele = data[--size];
            stackFull.signal();
            return ele;
        } finally {
            lock.unlock();
        }
    }

    public void pushLock(E ele) throws InterruptedException {
        lock.lock();
        try {
            while(size == data.length) {
                stackFull.await();
                System.out.println("Push woke up can work? " + (size != data.length));
            }
            data[size++] = ele;
            stackEmpty.signal();
        } finally {
            lock.unlock();
        }
    }
}
