package lesson4;

public class StackApp {



    public static void main(String[] args) {

        Stack<Integer> stack = new Stack<>(5);

        Runnable push = () -> {
            for(int i =0; i< 20; i++) {
                try {
                    stack.pushLock(i);
                    System.out.println("pushed " + i);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        Runnable pop = () -> {
            for(int i =0; i< 60; i++) {
                try {
                    System.out.println("popped" + stack.popLock());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        new Thread(push).start();
        new Thread(push).start();
        new Thread(push).start();
        new Thread(pop).start();
    }
}
