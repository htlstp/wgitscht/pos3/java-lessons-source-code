package lesson4.train;

class SingleTrackAwait implements Track {

    boolean isFree = true;

    public synchronized void enter() throws InterruptedException {
        while (!isFree) {
            wait();
        }
        isFree = false;
        notifyAll();
    }

    public synchronized void leave() throws InterruptedException {
        isFree = true;
        notifyAll();
    }
}
