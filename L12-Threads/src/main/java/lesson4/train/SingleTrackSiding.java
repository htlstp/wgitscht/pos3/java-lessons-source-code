package lesson4.train;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class SingleTrackSiding implements Track {

    boolean trackFree = true;
    boolean sidingFree = true;
    ReentrantLock lock = new ReentrantLock();
    Condition track = lock.newCondition();
    Condition siding = lock.newCondition();
    public void enter() throws InterruptedException {
        try {
            lock.lock();
            while(!trackFree) {
                track.await();
            }
        } finally {
            lock.unlock();
        }
    }

    public void enterSiding(Train train) throws InterruptedException {
        try {
            lock.lock();
            while(!sidingFree) {
                siding.await();
            }
            sidingFree = false;
            trackFree = true;
            track.signalAll();
            System.out.println(train.getLabel() + "in siding");
        } finally {
            lock.unlock();
        }
    }

    public void passSiding(Train train) throws InterruptedException {
        try {
            lock.lock();
            sidingFree = true;
            trackFree = false;
            siding.signalAll();
            System.out.println(train.getLabel() + "passed siding");
        } finally {
            lock.unlock();
        }
    }

    public void leave() throws InterruptedException {
        try {
            trackFree = true;
            lock.lock();
            track.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
