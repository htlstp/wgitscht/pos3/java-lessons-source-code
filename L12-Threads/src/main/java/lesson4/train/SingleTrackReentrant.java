package lesson4.train;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class SingleTrackReentrant implements Track {

    boolean isFree = true;
    ReentrantLock lock = new ReentrantLock();
    Condition free = lock.newCondition();
    public void enter() throws InterruptedException {

        lock.lock();
        try {
            while (!isFree) {
                free.await();
            }
            isFree = false;

        } finally {
            lock.unlock();
        }
    }

    public void leave() throws InterruptedException {
        lock.lock();
        try {
            isFree = true;
            free.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
