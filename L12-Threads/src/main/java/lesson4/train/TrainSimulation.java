package lesson4.train;

public class TrainSimulation {


    public static void main(String[] args) {
        Track track = new DoubleTrackSemaphore();
        Thread t0 = new Thread(new Train("[###] ", track));
        Thread t1 = new Thread(new Train("\t\t[***] ", track));
       // Thread t2 = new Thread(new Train("\t\t\t\t[+++] ", track));
        t0.start();
        t1.start();
        //t2.start();
    }
}


