package lesson4.train;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class DoubleTrackSemaphore implements  Track {

    private Semaphore semaphore = new Semaphore(2,true);

    @Override
    public void enter() throws InterruptedException {
        semaphore.acquire();
    }

    @Override
    public void leave() throws InterruptedException {
        semaphore.release();
    }
}
