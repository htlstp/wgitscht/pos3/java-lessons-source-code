package lesson4.train;

public interface Track {
    public void enter() throws InterruptedException;
    public void leave() throws InterruptedException;

    default void passSiding(Train train) throws InterruptedException {

    }

    default void enterSiding(Train train) throws InterruptedException {

    }
}
