package lesson4.train;

class Train implements Runnable {

    private final Track track;
    private final String label;

    public Train(String label, Track track) {
        this.track = track;
        this.label = label;
    }


    public String getLabel() {
        return label;
    }

    public void run() {
        try {
            for (int i = 0; i < 2; i++) {
                System.out.println(this.label + "running");
                Thread.sleep(1000);
            }
            System.out.println(this.label + "try to enter...");
            this.track.enter();
            System.out.println(this.label + "entered!");
            for (int i = 0; i < 3; i++) {
                System.out.println(this.label + "running on SingleTrack");
                Thread.sleep(300);
                this.track.enterSiding(this);
                Thread.sleep(1000);
                this.track.passSiding(this);
            }
            System.out.println(this.label + "exiting");
            this.track.leave();
            for (int i = 0; i < 2; i++) {
                System.out.println(this.label + "running");
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
}
