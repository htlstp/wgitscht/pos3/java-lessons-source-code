package lesson4.lager;

public class Lager {

    private boolean nachfrage;

    public int getBestand() {
        return bestand;
    }

    private int bestand = 10;
    private int transaktionen = 0;
    private final int MAX = 20;


    public synchronized void liefere(int anzahl){
        bestand += anzahl;
        transaktionen++;
        System.out.println(Thread.currentThread().getName()  +
                " liefert " + anzahl + ", neuer Bestand: " + bestand);
        notify();
    }



    public synchronized void verkaufe(int anzahl) {
        nachfrage = true;
        while (bestand < anzahl) {
            try {
                System.out.println("nicht genug vorhanden ("+anzahl+") angefragt vorhanden ("+bestand+")");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        bestand -= anzahl;
        System.out.println(Thread.currentThread().getName()  +  " kauft " + anzahl + ", neuer Bestand: " + bestand);
        nachfrage = false;
    }




    boolean istOffen() {
        if (transaktionen < MAX || nachfrage) {
            return true;
        } else {
            System.out.println(Thread.currentThread().getName()+ ", es ist geschlossen!");
            return false;
        }
    }
}
