package lesson4.lager;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created: 29.05.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class Kunde implements Runnable {
    private final Lager lager;

    public Kunde(Lager lager) {
        this.lager = lager;
    }

    @Override
    public void run() {
        while(lager.istOffen()) {
            lager.verkaufe(ThreadLocalRandom.current().nextInt(1, 21));
            sleepRandom();
        }
    }

    private void sleepRandom() {
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(100,800));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
