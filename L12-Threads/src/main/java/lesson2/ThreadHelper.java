package lesson2;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ThreadHelper {


    private ThreadHelper() {}
    private static final Random RD = new Random();

    public static void randomSleep(int maxSeconds) {
        try {
            TimeUnit.SECONDS.sleep(RD.nextInt(0,maxSeconds+1));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt(); // this is the way
        }
    }

    public static void sleepSafely(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
