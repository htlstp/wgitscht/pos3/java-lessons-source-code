package lesson2.reporter;

import lesson2.ThreadHelper;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Worker implements Runnable {

    @Override
    public void run() {
        var count = 3;
        while(count-- > 0) {
            ThreadHelper.randomSleep(3);
        }
    }
}
