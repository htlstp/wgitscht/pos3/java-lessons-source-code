package lesson2.reporter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class App {

    private List<Thread> runners = new ArrayList<>();
    public static void main(String[] args) {
      new App().start();
    }
    public void start(){

        IntStream.range(0,3).forEach(i -> runners.add(new Thread(new Worker(), "worker" + i)));
        Thread controller = new Thread(new ThreadController(runners));
        controller.start();
        runners.forEach(Thread::start);
    }
}
