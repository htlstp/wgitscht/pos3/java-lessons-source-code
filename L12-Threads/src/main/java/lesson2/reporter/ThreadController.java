package lesson2.reporter;

import lesson2.ThreadHelper;

import java.util.List;

public class ThreadController implements Runnable {


    private final List<Thread> threads;

    public ThreadController(List<Thread> threads) {
        this.threads = threads;
    }
    @Override
    public void run() {
        long alive;
        do {
            alive = threads.stream().filter(Thread::isAlive).count();
            System.out.printf("%d threads alive%n", alive);
            ThreadHelper.sleepSafely(3);
        } while(alive > 0);
    }


}
