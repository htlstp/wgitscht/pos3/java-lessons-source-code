package lesson2.mersenne;

import java.util.ArrayList;
import java.util.List;

public class FindMersennePrime implements Runnable {


    private final int from;
    private final int to;

    public FindMersennePrime(int from, int to){
        this.from = from;
        this.to = to;
    }

    private List<Integer> mersenne = new ArrayList<>();
    @Override
    public void run() {
        for(int i = from+1; i < to; i+=2){
            if(LucasLehmer.isPrime(i)) {
                mersenne.add(i);
            }
        }
    }

    public List<Integer> getMersenne() {
        return mersenne;
    }
}
