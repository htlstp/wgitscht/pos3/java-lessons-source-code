package lesson2.mersenne;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class App {


    private static final int THREADS = 128;
    private static final int MAX_EXP = 6000;

    public static void main(String[] args) throws InterruptedException {
      //  new App().start();
        System.out.println("pooled");
        new App().startPooled();
    }

    private void start() throws InterruptedException {

        var intervall = MAX_EXP / THREADS;

        var start = Instant.now();

        List<Integer> result = new ArrayList<>();
        List<Thread> threadList = new ArrayList<>();
        List<FindMersennePrime> workers = new ArrayList<>();
        for (int i = 0; i < MAX_EXP; i += intervall) {
            var worker = new FindMersennePrime(i, i + intervall);
            workers.add(worker);
            threadList.add(Thread.ofVirtual().unstarted(worker));
        }
        // start them
        threadList.forEach(Thread::start);
        for (Thread thread : threadList) {
            thread.join();
        }
        workers.forEach(i -> result.addAll(i.getMersenne()));
        System.out.println("result in " + Duration.between(start, Instant.now()).toMillis());
        result.sort(Comparator.naturalOrder());
        System.out.println(result);

    }

    private void startPooled() throws InterruptedException {
        AtomicInteger toTest = new AtomicInteger(1);

        List<Thread> threadList = new ArrayList<>();
        List<Integer> result = new CopyOnWriteArrayList<>();


        var start = Instant.now();

        for (int i = 0; i < THREADS; i++) {
            threadList.add(Thread.ofVirtual()
                    .unstarted((new FindMersenneFetch(toTest, MAX_EXP, result))));
        }
        // start them
        threadList.forEach(Thread::start);
        for (Thread thread : threadList) {
            thread.join();
        }
        System.out.println("result in " + Duration.between(start, Instant.now()).toMillis());
        result.sort(Comparator.naturalOrder());
        System.out.println(result);
    }
}
