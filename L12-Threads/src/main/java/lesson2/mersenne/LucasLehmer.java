package lesson2.mersenne;

import java.math.BigInteger;

public class LucasLehmer {

        private LucasLehmer(){
        }
        static boolean isPrime(int p) {
            BigInteger s = BigInteger.valueOf(4);
            BigInteger m = BigInteger.valueOf(2).pow(p).subtract(BigInteger.ONE);
            for (int i = 0; i < p - 2; i++)
                s = s.multiply(s).subtract(BigInteger.TWO).mod(m);
            return s.equals(BigInteger.ZERO);
        }
}
