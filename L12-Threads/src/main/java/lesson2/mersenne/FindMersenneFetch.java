package lesson2.mersenne;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class FindMersenneFetch implements Runnable {

    private final AtomicInteger next;
    private final int max;
    private final List<Integer> results;

    public FindMersenneFetch(AtomicInteger next, int max, List<Integer> results){
        this.next = next;
        this.max = max;
        this.results = results;
    }
    @Override
    public void run() {
        var current = max;
        while((current = next.addAndGet(2)) < max){
            if(LucasLehmer.isPrime(current))
                results.add(current);
        }
    }

}
