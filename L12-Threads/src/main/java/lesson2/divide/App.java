package lesson2.divide;

import java.util.ArrayList;
import java.util.List;

public class App {

    final static int MAX = 100_000;

    public static void main(String[] args) throws InterruptedException {
        new App().start(MAX);
    }

    private void start(int max) throws InterruptedException {
        int maxThreads = 20;
        var interval = max / maxThreads;

        List<Thread> threadList = new ArrayList<>();
        List<SumWorker> workers = new ArrayList<>();

        for (int i = 0; i < max; i += interval) {
            var worker = new SumWorker(i, i + interval);
            workers.add(worker);
            threadList.add(new Thread(worker));
        }
        // start them
        threadList.forEach(Thread::start);
        for (Thread thread : threadList) {
            thread.join();
        }
        var sum = workers.stream().mapToLong(SumWorker::getSum).sum();
    }
}
