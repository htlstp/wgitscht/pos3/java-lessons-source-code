package lesson2.divide;

import lesson2.ThreadHelper;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class SumWorker implements Runnable {


    private final int from;
    private final int to;
    long sum = 0;

    public SumWorker(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        ThreadHelper.randomSleep(3);
        this.sum = IntStream.range(from, to).sum();
    }

    public long getSum() {
        return sum;
    }
}
