package mypoi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


record Person(int id, String name, String lastname) {
}

public class WriteExcel {

    public static void main(String[] args) {


        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");

        //Prepare data to be written as an Object[]
        Set<Person> data = new HashSet<>();
        data.add(new Person(1, "Amit", "Shukla"));
        data.add(new Person(2, "Lokesh", "Gupta"));
        data.add(new Person(3, "John", "Adwards"));
        data.add(new Person(4, "Brian", "Schultz"));

        //Iterate over data and write to sheet

        int rownum = 0;
        Row header = sheet.createRow(rownum++);

        for (Person p : data) {
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            Cell cell = row.createCell(cellnum++);
            cell.setCellValue(p.name());
            Cell cell2 = row.createCell(cellnum++);
            cell2.setCellValue(p.lastname());
        }

        try {
            FileOutputStream out = new FileOutputStream(new File("demo_excel.xlsx"));
            workbook.write(out);
            out.close();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}