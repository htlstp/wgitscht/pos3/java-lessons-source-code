package proxy;

public class Proxy implements Subject {
    private RealSubject realService;

    public Proxy(){
        realService = new RealSubject();
    }

    @Override
    public void operation() {
        // check access, create a transaction, cache some values
        System.out.println("proxy operation");
        realService.operation();
    }
}
