package proxy.quoteservice;

public class QuoteProxy implements QuoteService {

    private final String userContext;
    private final QuoteServer quoteServer;

    public QuoteProxy(String userContext) {
        this.userContext = userContext;
        this.quoteServer = new QuoteServer();
    }

    @Override
    public String getQuote() {
        return quoteServer.getQuote();
    }

    @Override
    public void addQuote(String quote) {
        if(!"ADMIN".equals(userContext)) {
            throw new RuntimeException("You are not allowed to add a quote!");
        }
        quoteServer.addQuote(quote);
    }
}
