package proxy.quoteservice;

public interface QuoteService {
    public String getQuote();
    public void addQuote(String quote);
}
