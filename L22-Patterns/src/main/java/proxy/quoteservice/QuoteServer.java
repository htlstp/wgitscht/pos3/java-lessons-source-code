package proxy.quoteservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuoteServer implements QuoteService {

    private List<String> quotes = new ArrayList<String>();
    private Random rand = new Random();

    @Override
    public String getQuote() {
        return quotes.get(rand.nextInt(0, quotes.size()));
    }

    @Override
    public void addQuote(String quote) {
       quotes.add(quote);
    }
}
