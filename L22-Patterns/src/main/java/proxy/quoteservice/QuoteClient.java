package proxy.quoteservice;

public class QuoteClient {

    public static void main(String[] args) {
        QuoteService service = new QuoteProxy("ADMIN");
        service.addQuote("real artists ship");
        service.addQuote("do or do not, there is no try");
        System.out.println(service.getQuote());
        QuoteService fetchService = new QuoteProxy("USER");
        System.out.println(service.getQuote());
    }
}
