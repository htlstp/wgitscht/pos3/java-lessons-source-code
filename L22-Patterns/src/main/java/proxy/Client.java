package proxy;

public class Client {

    Subject subject;

    Client(Subject subject) {
        this.subject = subject;
    }

    public void doSomething(){
        subject.operation();
    }
}
