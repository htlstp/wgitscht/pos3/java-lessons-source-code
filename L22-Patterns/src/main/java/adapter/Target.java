package adapter;

public interface Target {
    void operation();
}
