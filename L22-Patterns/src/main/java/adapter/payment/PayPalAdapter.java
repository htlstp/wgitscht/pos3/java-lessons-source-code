package adapter.payment;

public class PayPalAdapter implements PaymentProvider {

    PayPalPayment payPalPayment = new PayPalPayment();
    @Override
    public boolean processPayment(double amount) {
        return payPalPayment.schedulePayment(amount);
    }
}
