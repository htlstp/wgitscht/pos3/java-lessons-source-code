package adapter.payment;

public interface PaymentProvider {
    boolean processPayment(double amount);
}
