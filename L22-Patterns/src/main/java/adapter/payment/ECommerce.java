package adapter.payment;

public class ECommerce {

    private PaymentProvider paymentProvider;

    public ECommerce(PaymentProvider paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public boolean pay(double amount){
        return paymentProvider.processPayment(amount);
    }
}
