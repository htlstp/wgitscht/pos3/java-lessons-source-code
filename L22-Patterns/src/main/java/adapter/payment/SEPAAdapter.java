package adapter.payment;

public class SEPAAdapter implements PaymentProvider {
    private SEPAPayment sepaPayment = new SEPAPayment();

    @Override
    public boolean processPayment(double amount) {
        return sepaPayment.sepaTransfer(amount);
    }
}
