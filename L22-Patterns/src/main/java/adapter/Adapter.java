package adapter;

public class Adapter implements Target {

    private Adaptee adaptee = new Adaptee();

    @Override
    public void operation() {
        adaptee.specificOperation();
    }
}
