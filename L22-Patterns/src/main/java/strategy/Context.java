package strategy;

public class Context {

    private Strategy strategy;


    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void compute() {
        if(strategy == null) { throw new IllegalStateException("no strategy defined");}
        strategy.algorithm();
    }

}
