package strategy;

public class Client {
    public static void main(String[] args) {
        Context context = new Context();
        context.setStrategy(new StrategyA());
        context.compute();
        context.setStrategy(new StrategyB());
        context.compute();
    }
}
