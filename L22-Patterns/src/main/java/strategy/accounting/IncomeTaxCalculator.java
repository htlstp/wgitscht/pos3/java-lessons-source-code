package strategy.accounting;

public class IncomeTaxCalculator implements TaxCalculator {
    @Override
    public double calculateTax(double v) {
        return v * 0.3;
    }
}
