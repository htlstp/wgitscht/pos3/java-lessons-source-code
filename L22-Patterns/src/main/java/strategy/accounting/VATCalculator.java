package strategy.accounting;

public class VATCalculator implements TaxCalculator {
    @Override
    public double calculateTax(double v) {
        return v * 0.2;
    }
}
