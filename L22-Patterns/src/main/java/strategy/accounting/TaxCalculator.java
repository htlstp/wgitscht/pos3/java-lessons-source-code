package strategy.accounting;

public interface TaxCalculator {
    double calculateTax(double v);
}
