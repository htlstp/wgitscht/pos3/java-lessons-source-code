package strategy.accounting;

public class SalesTaxCalculator implements TaxCalculator {
    @Override
    public double calculateTax(double v) {
        return v*0.1;
    }
}
