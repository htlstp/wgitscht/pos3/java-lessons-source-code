package strategy.accounting;

import java.util.Arrays;
import java.util.function.Function;

public class AccountingApp {

    private TaxCalculator taxCalculator;
    private Function<Double, Double> lambdaCalculator;

    public AccountingApp(TaxCalculator taxCalculator) {
        this.taxCalculator = taxCalculator;
    }

    public double calculateTax(double[] amounts) {
        return Arrays.stream(amounts)
                .map(taxCalculator::calculateTax)
                .sum();
    }

    public void setTaxCalculator(TaxCalculator taxCalculator) {
        this.taxCalculator = taxCalculator;
    }




    public static void main(String[] args) {
        double[] amounts = {100, 200, 300};



        AccountingApp accountingApplication = new AccountingApp(new IncomeTaxCalculator());
        System.out.println("Total Income Tax: " + accountingApplication.calculateTax(amounts));

        accountingApplication.setTaxCalculator(new VATCalculator());
        System.out.println("Total VAT Tax: " + accountingApplication.calculateTax(amounts));

        accountingApplication.setTaxCalculator(new SalesTaxCalculator());
        System.out.println("Total Sales Tax: " + accountingApplication.calculateTax(amounts));

        accountingApplication.setTaxCalculator((v) -> v * 0.8);
        System.out.println("Total Income Tax: " + accountingApplication.calculateTax(amounts));
    }
}