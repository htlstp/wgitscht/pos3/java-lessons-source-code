package strategy;

public class StrategyB implements Strategy{
    @Override
    public void algorithm() {
        System.out.println("Algorithm B");
    }
}
