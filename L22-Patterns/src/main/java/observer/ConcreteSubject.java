package observer;

import java.util.ArrayList;
import java.util.List;

public class ConcreteSubject implements Observable {

    private String value;
    private List<Observer> observerList = new ArrayList<>();

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        updateAll();
    }

    @Override
    public void register(Observer o) {
        observerList.add(o);
    }

    @Override
    public void unregister(Observer o) {
        observerList.remove(o);
    }

    @Override
    public void updateAll() {
        observerList.forEach(Observer::update);
    }
}
