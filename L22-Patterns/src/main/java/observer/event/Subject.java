package observer.event;

public class Subject {


    String value;
    public EventManager<String> eventManager = new EventManager<>();

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        eventManager.notifyAll(value);
    }
}
