package observer.event;

public class Demo {

    public static void main(String[] args) {
        Subject s = new Subject();
        s.eventManager.register(System.out::println);
        s.eventManager.register(System.out::println);
        s.setValue("foo");
    }
}
