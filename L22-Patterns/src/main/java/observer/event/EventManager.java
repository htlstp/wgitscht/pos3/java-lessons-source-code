package observer.event;

import java.util.ArrayList;
import java.util.List;

public class EventManager<T> {

    List<EventListener<T>> listeners = new ArrayList<>();

    public void register(EventListener<T> listener) {
        listeners.add(listener);
    }

    public void notifyAll(T change){
        listeners.forEach(l -> l.update(change));
    }

}
