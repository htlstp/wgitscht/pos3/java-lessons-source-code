package observer.event;

public interface EventListener<T> {
    void update(T t);
}
