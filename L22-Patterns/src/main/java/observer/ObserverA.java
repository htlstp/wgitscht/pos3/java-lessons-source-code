package observer;

public class ObserverA implements Observer {

    private ConcreteSubject subject;

    public ObserverA(ConcreteSubject s) {
        subject = s;
        subject.register(this);
    }
    @Override
    public void update() {
        System.out.println("Observer A" + subject.getValue());
    }

    public void unregister() {
        subject.unregister(this);
    }
}
