package observer;

public class ObserverB implements Observer {

    private ConcreteSubject subject;

    public ObserverB(ConcreteSubject s) {
        subject = s;
        subject.register(this);
    }
    @Override
    public void update() {
        System.out.println("Observer B" + subject.getValue());
    }
}