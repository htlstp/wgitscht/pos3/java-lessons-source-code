package observer;

public class Demo {

    public static void main(String[] args) {
        ConcreteSubject cs = new ConcreteSubject();
        ObserverA oa = new ObserverA(cs);
        new ObserverB(cs);
        cs.setValue("foobar");
        cs.setValue("bar");
        oa.unregister();
        cs.setValue("only B");
    }
}
