package at.htlstp.dbutil;

import java.sql.Connection;
import java.sql.SQLException;
public class AutoSetAutoCommit implements AutoCloseable {

    private Connection connection;
    private boolean wasAutoCommit;

    public AutoSetAutoCommit(Connection connection, boolean withAutoCommit) throws SQLException {
        this.connection = connection;
        wasAutoCommit = connection.getAutoCommit();
        connection.setAutoCommit(withAutoCommit);
    }

    @Override
    public void close() throws SQLException {
        connection.setAutoCommit(wasAutoCommit);
    }

}
