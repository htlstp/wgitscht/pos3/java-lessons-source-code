package at.htlstp.dbutil;

import java.sql.Connection;
import java.sql.SQLException;

public class AutoRollbackOrCommit implements AutoCloseable {

    private Connection connection;
    private boolean committed;

    public AutoRollbackOrCommit(Connection connection) throws SQLException {
        this.connection = connection;
    }

    public void commit() throws SQLException {
        connection.commit();
        committed = true;
    }

    @Override
    public void close() throws SQLException {
        if(!committed) {
            connection.rollback();
        }
    }

}