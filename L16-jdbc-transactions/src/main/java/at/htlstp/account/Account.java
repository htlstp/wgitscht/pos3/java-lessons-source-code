package at.htlstp.account;

import java.util.Objects;

public class Account {

    private Integer number;
    private int balance;
    private String name;

    public Account(Integer number, int balance, String name) {
        this.number = number;
        this.balance = balance;
        this.name = name;
    }

    public Account(int balance, String name) {
        this(null, balance, name);
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public int getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return balance == account.balance && Objects.equals(number, account.number) && Objects.equals(name, account.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, balance, name);
    }
}
