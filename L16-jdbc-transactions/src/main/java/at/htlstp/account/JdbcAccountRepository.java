package at.htlstp.account;

import java.sql.*;
import java.util.List;
import java.util.Optional;

public class JdbcAccountRepository implements AccountRepository {

    private final Connection connection;

    public JdbcAccountRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<Account> findAccountById(int id) throws SQLException {
        var sql = """
                SELECT id,balance,name from accounts
                where id = ?
                """;
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return Optional.of(new Account(
                        rs.getInt("id"),
                        rs.getInt("balance"),
                        rs.getString("name")));
            }
        }
        return Optional.empty();
    }

    @Override
    public List<Account> saveAll(List<Account> accounts) throws SQLException {
        var sql = """
                INSERT INTO accounts (balance, name) VALUES (?,?)
                """;
        connection.setAutoCommit(false);
        try(PreparedStatement preparedStatement =  connection.prepareStatement(sql,
                Statement.RETURN_GENERATED_KEYS)){

            for(Account a : accounts) {
                preparedStatement.setInt(1, a.getBalance());
                preparedStatement.setString(2, a.toString());
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();
            var keys = preparedStatement.getGeneratedKeys();
            var idx = 0;
            while(keys.next()) {
                accounts.get(idx++).setNumber(keys.getInt(1));
            }
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.setAutoCommit(true);
        }
        return accounts;
     }

    @Override
    public void transferMoney(Account source, Account destination, int amount) throws SQLException {
        connection.setAutoCommit(false);
        var sql = """
                UPDATE accounts 
                SET balance = balance + ? 
                WHERE id = ? """;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, amount);
            statement.setInt(2, destination.getNumber());
            statement.executeUpdate();

            statement.setInt(1, -amount);
            statement.setInt(2, source.getNumber());
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.setAutoCommit(true);
        }
    }
}
