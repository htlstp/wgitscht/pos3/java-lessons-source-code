package at.htlstp.account;

import at.htlstp.account.Account;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface AccountRepository {

    List<Account> saveAll(List<Account> accounts) throws SQLException;

    void transferMoney(Account a, Account b, int amount) throws SQLException;
    Optional<Account> findAccountById(int id) throws SQLException;
}
