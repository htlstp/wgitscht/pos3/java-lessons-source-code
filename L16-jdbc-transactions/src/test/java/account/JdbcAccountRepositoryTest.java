package account;


import at.htlstp.account.Account;
import at.htlstp.account.AccountRepository;
import at.htlstp.account.JdbcAccountRepository;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import setup.TestConnectionSupplier;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class JdbcAccountRepositoryTest {

    private AccountRepository accountRepository;
    private Connection connection;

    @BeforeEach
    void createRepository() throws SQLException {
        connection = TestConnectionSupplier.getConnection();
        accountRepository = new JdbcAccountRepository(connection);
    }
    @AfterEach
    void closeCon() throws SQLException {
        connection.close();
    }

    @Test
    void testFindById() throws SQLException {
        assertThat(accountRepository.findAccountById(1))
                .hasValue(new Account(1, 100, "a"));
    }


    @Test
    void testBatchInsertGeneratesKeys() throws SQLException {
        Account a = new Account(100, "a");
        Account b = new Account(200,  "b");
        Account c = new Account(300,  "c");
        assertThat(accountRepository.saveAll(Arrays.asList(a,b,c)))
                .extracting(Account::getNumber)
                .doesNotContainNull();
    }


    @Test
    void testTransferMoneyAddsAndSubstracts() throws SQLException {
        Account a = new Account(1, 100, "a");
        Account b = new Account(2, 200, "b");
        accountRepository.transferMoney(a, b, 20);

        assertThat(accountRepository.findAccountById(a.getNumber()))
                .map(Account::getBalance).hasValue(80);
        assertThat(accountRepository.findAccountById(b.getNumber()))
                .map(Account::getBalance).hasValue(220);

    }


    @Test
    void testTransferMoneyRollback() throws SQLException {
        Account a = new Account(1, 100, "a");
        Account b = new Account(2, 200, "b");

        accountRepository.transferMoney(a, b, 200);

        assertThat(accountRepository.findAccountById(a.getNumber()))
                .map(Account::getBalance).hasValue(100);
        assertThat(accountRepository.findAccountById(b.getNumber()))
                .map(Account::getBalance).hasValue(200);

    }
}
