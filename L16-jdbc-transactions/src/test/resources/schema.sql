CREATE TABLE accounts
(
    id IDENTITY NOT NULL PRIMARY KEY,
    name    VARCHAR(100) NOT NULL,
    balance INT not null default 0
);

alter table accounts add constraint balance_not_neg check(balance >= 0);

insert into accounts (name, balance) values ('a',100);
insert into accounts (name, balance) values ('b',200);