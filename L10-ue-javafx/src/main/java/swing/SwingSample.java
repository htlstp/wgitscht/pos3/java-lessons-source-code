package swing;
import javax.swing.*;
/**
 * Created: 24.02.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class SwingSample {
    public static void main(String[] args) {
        JFrame f=new JFrame();

        JButton b= new JButton("click");
        b.setBounds(130,100,100, 40);
        f.add(b);
        f.setSize(400,500);
        f.setLayout(null);
        f.setVisible(true);
    }
}
