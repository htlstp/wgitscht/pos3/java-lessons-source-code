package fx;

import javafx.application.Application;
import javafx.beans.property.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;

/**
 * Created: 24.02.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class FxWithProperties extends Application {
    public static final Logger LOGGER = LoggerFactory.getLogger(FxWithProperties.class);



    @Override
    public void start(Stage stage) {
        IntegerProperty counter = new SimpleIntegerProperty(1);
        DoubleProperty db;
        StringProperty sp;
        FloatProperty fp;

        int counterValue = counter.get();
        System.out.println("Counter:" + counterValue);
        counter.set(2);
        counterValue = counter.get();
        System.out.println("Counter:" + counterValue);
    }
}
