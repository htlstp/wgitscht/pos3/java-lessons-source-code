package fx;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created: 24.02.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class FxWithBindings extends Application {
    public static final Logger LOGGER = LoggerFactory.getLogger(FxWithBindings.class);

    @Override
    public void start(Stage stage) {
        final Circle c = new Circle(10.0);
        System.out.println(c);
        c.setRadius(12.0);
        System.out.println(c);
        c.radiusProperty().set(13.0);
        System.out.println(c);
        System.out.println(c.radiusProperty().toString());
        System.out.println("-----------------------");
        DoubleProperty rp = c.radiusProperty();
        System.out.println("Wert der Property: " + rp.getValue());
        System.out.println("Wert der Property: " + rp.get());
        System.out.println("Name der Property: " + rp.getName());
        System.out.println("Bean der Property: " + rp.getBean());
        c.setFill(Color.RED);
        System.out.println("-----------------------");
        ObjectProperty<Paint> fp = c.fillProperty();
        System.out.println("Wert der Property: " + fp.getValue());
        System.out.println("Wert der Property: " + fp.get());
        System.out.println("Name der Property: " + fp.getName());
        System.out.println("Bean der Property: " + fp.getBean());
    }
}
