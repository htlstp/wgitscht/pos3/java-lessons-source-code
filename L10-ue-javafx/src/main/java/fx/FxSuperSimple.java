package fx;
/**
 * Created: 24.02.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FxSuperSimple extends Application {
    @Override
    public void start(Stage stage) {
        // add scene (group)
        // show bg color
        // show fullscreen
        // show without scene
        // add icon
        stage.show();
    }
}
