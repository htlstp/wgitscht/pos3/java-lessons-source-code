package fx.firstApp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class FxFirstAppGreeterFXML extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = getClass().getResource("/layouts/greeter.fxml");
        Parent parent = FXMLLoader.load(url);
        primaryStage.setTitle("With FXML");
        primaryStage.setScene(new Scene(parent));
        primaryStage.show();
    }
}
