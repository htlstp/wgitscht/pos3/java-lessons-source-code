package fx.firstApp;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class GreetingController {
    @FXML
    private TextField nameInput;
    @FXML
    private Button buttonGreet;
    @FXML
    private TextField greetingOutput;



    public void initialize() {
        System.out.println("modern way of initializing");
        buttonGreet.setOnMouseClicked(this::greet);
    }

    private void greet(MouseEvent mouseEvent) {
        if(nameInput.getText().isBlank()) {
            greetingOutput.setText("Bitte den Namen angeben");
        } else {
            greetingOutput.setText("Hello "+nameInput.getText());
        }
    }
}
