package fx.firstApp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class FxFirstAppGreeter extends Application {

    private final Button greetButton = new Button("Grüßen");
    private final TextField nameInput = new TextField();
    private final Label labelForNameInput = new Label("Namen eingeben:");
    private final TextField greetOutput = new TextField();

    @Override
    public void start(Stage primaryStage) {
        GridPane root = new GridPane();
        greetButton.setOnMouseClicked(this::greet);
        root.add(labelForNameInput, 1,0);
        root.add(nameInput,2,0);
        root.add(greetButton,1,1);
        root.add(greetOutput, 2,1);
        root.setHgap(20);
        root.setVgap(5);
        root.setGridLinesVisible(true);
        Scene scene = new Scene(root);
        primaryStage.setWidth(600);
        primaryStage.setHeight(400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void greet(MouseEvent mouseEvent) {
        if(nameInput.getText().isBlank()) {
            greetOutput.setText("Bitte den Namen angeben");
        } else {
            greetOutput.setText("Hello "+nameInput.getText());
        }
    }

}
