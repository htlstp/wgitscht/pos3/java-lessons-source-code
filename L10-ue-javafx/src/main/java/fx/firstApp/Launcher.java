package fx.firstApp;

import fx.finished.FxSuperSimple;
import javafx.application.Application;

public class Launcher {
    public static void main(String[] args) {
        Application.launch(FxFirstAppGreeterFXML.class);
    }
}
