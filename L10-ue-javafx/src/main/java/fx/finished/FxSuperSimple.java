package fx.finished;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Objects;

public class FxSuperSimple extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("starting");
        VBox root = new VBox();
        Scene scene = new Scene(root, Color.BLACK);
        primaryStage.setHeight(420);
        primaryStage.setWidth(420);
    //    primaryStage.setFullScreen(true);
        primaryStage.setScene(scene);
        root.getChildren().add(new Button("hello"));

        Image htlLogo = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/images/H.png")));
        primaryStage.getIcons().add(htlLogo);
        primaryStage.show();
    }


}
