package awt;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created: 24.02.2022
 *
 * @author Werner Gitschthaler (Werner)
 */
public class AwtSample extends Frame {

    // initializing using constructor
    AwtSample() {

        // creating a button
        Button b = new Button("click");
        // setting button position on screen
        b.setBounds(30, 100, 80, 30);
        // adding button into frame
        add(b);
        // frame size 300 width and 300 height
        setSize(300, 300);
        // setting the title of Frame
        setTitle("This is AWT");
        setLayout(new FlowLayout());
        // now frame will be visible, by default it is not visible
        setVisible(true);
        addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        AwtSample f = new AwtSample();
    }

}
