import java.util.stream.IntStream;

void main(){ // preview features
    System.out.println(stringTemplates("code"));
}


public static String stringTemplates(String foo){
    return STR."""
        oh yes
     we can
    -> \{foo}
    syntax is ...
    """;
}

public static String unnamedPatterns(Object p) {
    if(p instanceof Pupil(String lastName, _ , _)) {
        return lastName;
    }
    return "not a pupil";
}

public static void unnamedVars() {
    try {
        var x = 3;
    } catch (Exception _) { // clear it is ignored}
    }
}
