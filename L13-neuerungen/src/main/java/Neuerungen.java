import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Neuerungen {

    public static void main(String[] args) {

        //
        System.out.println(patternMatchingInstanceOf("foobar"));

        // switch
        System.out.println(switchWithPatternMatching(20));
        System.out.println(switchWithPatternMatching(3));
        System.out.println(switchWithPatternMatching(3d));
        System.out.println(switchWithPatternMatching(null));


        sequencedCollection();
        // record Pattern Matching
        System.out.println(patternMatchingRecords(
                new Pupil("GITS", "Werner", new School("HTL", "PTown"))));
        System.out.println(patternMatchingRecords(new Teacher("REFR")));

        // text blocks
        System.out.println(textBlocks());
    }


    private static void withSwitchExpression(Fruit fruit) {
        switch (fruit) {
            case APPLE, PEAR -> System.out.println("Common fruit");
            case ORANGE, BANANA -> System.out.println("Exotic fruit");
            default -> System.out.println("Undefined fruit");
        }
    }


    public static String patternMatchingInstanceOf(Object o) {
        if (o instanceof String x && x.length() > 3) {
            return "we found it is a long string";
        }
        return "we can't classify!";
    }

    public static String switchWithYield(String some) {
        return switch (some) {
            case "foo" -> {
                var x = some.toUpperCase();
                yield x;
            }
            case "bar" -> some.substring(0, 1);
            default -> "i don't know this";
        };
    }

    public static String ifAndInstanceOf(Object o) {
        if (o instanceof Integer i) {
            return "o is an Integer with value " + i;
        }
        return "not an integer";
    }

    public static String switchInstanceOf(Object o) {
        return switch (o) {
            case Integer i -> String.format("%d is an Integer", i);
            case Double d -> String.format("%.2f is a Double", d);
            default -> String.format("%s it's a different type", o);
        };
    }


    private static void sequencedCollection() {
        List<String> sc = Stream.of("Alpha", "Bravo", "Charlie", "Delta")
                .collect(Collectors.toCollection(ArrayList::new));
        System.out.println("Initial list: " + sc);
        System.out.println("Reversed list: " + sc.reversed());
        System.out.println("First item: " + sc.getFirst());
        System.out.println("Last item: " + sc.getLast());
        sc.addFirst("Before Alpha");
        sc.addLast("After Delta");
        System.out.println("Added new first and last item: " + sc);
    }

    public static String switchWithPatternMatching(Object o) {
        return switch (o) {
            case Integer i when i > 10 -> String.format("%d is a big Integer", i);
            case Integer i -> String.format("%d is an Integer", i);
            case Double d -> String.format("%.2f is a Double", d);
            case null -> "you gave me null";
            default -> String.format("%s it's a different type", o);
        };
    }

    public static String textBlocks() {
        return """
                <html>
                    <head>
                        <title>Oh yes</title>
                    </head>
                    <body>
                        yes we can!
                    </body>
                </html>
                """;
    }

    public static String patternMatchingRecords(Object someObject) {
        if (someObject instanceof Pupil(String lastName, String firstName, School(String schoolName, String town))
                && lastName != null) {
            return lastName + " " + schoolName;
        }
        return "not a Pupil";
    }


    public static String patternMatchingRecordsSwitch(Object someObject) {
        return switch (someObject) {
            case Teacher(String name) when name.length() > 3 -> "oh foo";
            case Integer i -> " " + i;
            default -> "oh noes";
        };
    }
}


record Teacher(String name) {
}

record Pupil(String lastName, String firstName, School school) {
}

record School(String name, String town) {
}


enum Fruit {
    APPLE, PEAR, ORANGE, BANANA
}

abstract sealed class Vehicle permits Car, Bike {
}

final class Car extends Vehicle {
}

final class Bike extends Vehicle {
}

